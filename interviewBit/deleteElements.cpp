// Given an integer array A of size N.
// Find the minimum number of elements that need to be removed such that the GCD of the resulting array becomes 1.

// If not possible then return -1.

// Problem Constraints
// 1 <= N <= 100000
// 1 <= A[i] <= 1e9

//  A = [7, 2, 5]
//  0

#include<bits/stdc++.h>
using namespace std;

class Solution{

    public:
        int solve(vector<int>&A);
};
// [ 15, 30 ] => 0
int gcd(int a,int b) {
    if(a==0) return b;
    return gcd(b%a,a);
}

int Solution::solve(vector<int>&A) {
    int n = A.size();
    sort(A.begin(),A.end());
    int g = A[0];
    for(int i=0;i<n;i++) {
        g = gcd(g,A[i]);
        if(g == 1) return 0;
    }
    return -1;
}

int main() {
    int n; cin>>n;
    vector<int>v(n);
    for(int i=0;i<n;i++) cin>>v[i];
    Solution s;
    cout<<s.solve(v);
    return 0;
}

