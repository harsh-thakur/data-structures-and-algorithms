
let T = [],L=[];
function update(node,s,e,l,r){
    if(L[node]!=0){
        T[node] = (e-s+1) - T[node];
        if(s!=e){
            L[node*2] ^= 1;
            L[node*2 + 1] ^=1;
        }
        l[node] = 0;
    }

    if(s>e || e<l || r<s) return;
    if(s>=l && e<=r){
        T[node] = (e-s+1) - T[node];
        if(s!=e){
            L[node*2] ^= 1;
            L[node*2 + 1] ^=1;
        }
        return;
    }
    let mid = parseInt((s+e)/2);
    update(node*2,s,mid,l,r);
    update(node*2+1,mid+1,e,l,r);

    T[node] = T[node*2] + T[node*2 +1];
}

function query(node,s,e,l,r){
    // console.log(node,s,e,l,r);
    if(L[node]!=0){
        T[node] = (e-s+1) - T[node];
        if(s!=e){
            L[node*2] ^= 1;
            L[node*2 + 1] ^=1;
        }
        L[node] = 0;
    }
    if(s>e || e<l || r<s) return 0;
    if(s>=l && e<=r){
        return T[node];
    }
    let mid = parseInt((s+e)/2);
    let p1 = query(node*2,s,mid,l,r);
    let p2 = query(node*2 + 1,mid+1,e,l,r);
    return p1+p2;
}
function reset(size_t){
    T.length = size_t +1;
    L.length = size_t + 1;
    for(let i=0;i<=size_t;i++){
        T[i] = 0;
        L[i] = 0;
    }
}

function main(){
    const myArgs = process.argv.slice(2);
    const noOfCoins = myArgs[0];
    let queries = myArgs[1];

    const x = parseInt(Math.ceil(Math.log2(parseInt(noOfCoins))));
    const size_t = 2*Math.pow(2,x) - 1;
    reset(size_t);
    let i = 2;
    while(queries--){
        let type = myArgs[i++];
        let A = myArgs[i++];
        let B = myArgs[i++];
        if(type==0){
            update(1,0,noOfCoins - 1,parseInt(A),parseInt(B));
        }else{
            let ans = query(1,0,noOfCoins-1,parseInt(A),parseInt(B));
            console.log(ans);
        }
    }
}

main();