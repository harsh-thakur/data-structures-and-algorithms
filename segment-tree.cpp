#include <bits/stdc++.h>
using namespace std;

#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0);
#define endl "\n"
#define int long long
#define f(i, a, b) for (i = a; i < b; i++)
#define rep(i, n) f(i, 0, n)
#define fd(i, a, b) for (i = a; i >= b; i--)
#define pb push_back
#define mp make_pair
#define vi vector<int>

int n,q;
const int N = 1e5;
int t[2 * N];
int lazy[2*N];
vector<int>v;

void build(int ss, int se, int si) {
    if(ss > se) return; 
    if(ss==se) {
        t[si] = v[ss]; return;
    }
    int mid = (ss + se)/2;
    build(ss, mid, si*2+1);
    build(mid+1,se,si*2+2);
    t[si] = 0;
}

void updateUtil(int si, int ss, int se, int l, int r, int val) {
    // cout<<" update"<<si<<" "<<ss<<" "<<se<<" "<<l<<" "<<r<<" "<<val<<" "<<endl;
    if(lazy[si]!=0) {
        if(ss!=se) {
            lazy[si*2+1] = lazy[si*2+1] !=0 ? lazy[si*2+1] & val: val;
            lazy[si*2+2] = lazy[si*2+2] !=0 ? lazy[si*2+2] & val: val;
        }
        lazy[si] = 0;
    }
    if(ss > se || ss > r || se < l) return;
    if( l <= ss && se <= r) {
        if(se != ss)  {
            lazy[si*2+1] = lazy[si*2+1] !=0 ? lazy[si*2+1] & val: val;
            lazy[si*2+2] = lazy[si*2+2] !=0 ? lazy[si*2+2] & val: val;
        }
        return;
    }
    int mid = (ss+se)/2;
    updateUtil(si*2+1, ss, mid,l,r,val);
    updateUtil(si*2+2,mid+1,se,l,r,val);
}

void update(int l,int r, int val) {
    updateUtil(0,0,n-1,l,r,val);
}

int32_t main()
{
    /* code */
    cin >> n >> q;
    // vector<int> v(n);
    for (int i = 0; i < n; i++){
        int x; cin>>x;v.push_back(x);
    }
    build(0,n-1,0);
    int x = (int)(ceil(log2(n)));
    int max_size = 2*(int)pow(2, x) - 1; 


    for(int i=0;i<max_size;i++) cout<<t[i] <<" ";
    while(q--) {
        int l,r,val; cin>>l>>r>>val;
        update(l,r,val);
    }

    for(int i=0;i<max_size;i++) cout<<lazy[i] <<" ";

    return 0;
}