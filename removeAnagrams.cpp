#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

void funWithAnagrams(vector<string>v, int n) {
    vector<string>ans;
    int m[n][26];
    for(int i=0;i<n;i++) {
        for(int j=0;j<26;j++) {
            m[i][j]=0;
        }
    }
    for(int i=0;i<n;i++) {
        for(int j=0;j<v[i].size();j++) {
            m[i][v[i][j]-'a']++;
        }
    }
    ans.push_back(v[0]);
    for(int i=1; i<n; i++ ){
        for(int k= i-1; k>=0; --k) {
            bool isAnagram = true;
            for(int j=0;j<v[i].size();j++) {
                if(m[k][v[i][j]-'a'] != m[i][v[i][j]-'a']) {
                    isAnagram = false;
                    break;
                }
            }
            if(isAnagram) {
                break;
            }
            if(k==0 && !isAnagram) {
                ans.push_back(v[i]);
            }
        }
    }
    for(int i=0;i<ans.size();i++) {
        cout<<ans[i]<<" ";
    }
}


int32_t main() {
    // int t; cin>>t;
    // while (t--) {
        /* code */
         // [code, doce, aangarsm,]
         int n; cin>>n;
         vector<string>v(n);
         for(int i=0;i<n;i++) cin>>v[i];
         funWithAnagrams(v, n);
    // }
    
    return 0;
}