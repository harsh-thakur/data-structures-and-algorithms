
t = int(input())


def solve(C, L):
    currentCost = C[0]
    minimumCost = C[0]*L[0]
    i = 1
    while i < len(C):
        if currentCost < C[i]:
            minimumCost += currentCost*L[i]
        else:
            minimumCost += C[i]*L[i]
            currentCost = C[i]
        i += 1

    return minimumCost


while t:
    t -= 1
    N = int(input())
    C = list(map(int, input().split()))
    L = list(map(int, input().split()))
    print(solve(C, L))




# 8 3 5 9 10
# 1 4 5 6 9