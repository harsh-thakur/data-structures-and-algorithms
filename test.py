import sys

t = int(input())


def dist(x1, y1, x2, y2):
    return ((x1-x2)**2 + (y1-y2)**2)**0.5


while t:
    t -= 1
    x, y = map(int, input().split())
    n, m, k = map(int, input().split())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    c = list(map(int, input().split())) 
    low = sys.maxsize
    for i in range(n):
        probable_low = dist(x, y, a[2*i], a[2*i+1])
        if(probable_low < low):
            for j in range(m):
                step1_low = probable_low + dist(a[2*i], a[2*i+1], b[2*j], b[j*2+1])
                if step1_low < low:
                    for l in range(k):
                        step2_low = step1_low + dist(b[2*j], b[j*2+1], c[2*l], c[l*2+1])
                        if step2_low < low:
                            low = step2_low
    low_2 = sys.maxsize
    for i in range(m):
        probable_low = dist(x, y, b[2*i], b[2*i+1])
        if(probable_low < low_2):
            for j in range(n):
                step1_low = probable_low + dist(b[2*i], b[2*i+1], a[2*j], a[2*j+1])
                if step1_low < low_2:
                    for l in range(k):
                        step2_low = step1_low + dist(a[2*j], a[j*2+1], c[2*l], c[l*2+1])
                        if step2_low < low_2:
                            low_2 = step2_low

    print(min(low, low_2))                    