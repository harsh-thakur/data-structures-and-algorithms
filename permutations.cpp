#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >

void permute(string str, int l, int r) {
    if(l==r) cout<<str<<endl;
    for(int i=l; i<=r; i++) {
        char c = str[l];
        str[l] = str[i];
        str[i] = c;
        permute(str,l+1,r);
        c = str[l];
        str[l] = str[i];
        str[i] = c;
    }
}

int32_t main() {
    IOS
    string str; cin>>str;
    permute(str,0,str.size()-1);
    return 0;
}