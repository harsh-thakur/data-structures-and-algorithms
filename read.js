const fs = require('fs');

// fs.readFile('seller-details.json', () => {
//     setTimeout(() => {
//       console.log('timeout');
//     }, 0);
//     setImmediate(() => {
//       console.log('immediate');
//     });
//   });

  setTimeout(() => {
    console.log('without I/o timeout');
  }, 0);
  setImmediate(() => {
    console.log('without I/o  immediate');
  });