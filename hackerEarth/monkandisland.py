from collections import defaultdict


class Graph:
    def __init__(self, nodes):
        self.V = nodes
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    def bfs(self, s, d, dist):
        q = []
        visited = [False]*self.V
        q.append(s)
        visited[s] = True
        while q:
            s = q.pop(0)
            for i in self.graph[s]:
                if visited[i] == False:
                    q.append(i)
                    dist[i] = dist[s] + 1
                    visited[i] = True

                    if i == d:
                        return True
        return False


te = int(input())
for i in range(te):
    n, m = list(map(int, input().split()))
    g = Graph(n+1)
    for i in range(m):
        x, y = list(map(int, input().split()))
        g.addEdge(x, y)
    dist = [0]*(n+1)
    if g.bfs(1, n, dist):
        print(dist[n])
