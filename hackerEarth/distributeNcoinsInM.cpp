
// you have to distribute n coins among m people such that no person receives more than c coins. however a person can receive zero coins


#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


long long C(int n,int m){ 
	if(n<0||m<0||m>n)return 0; 
	long long ans=1; 
	for(int i=m+1;i<=n;i++)ans*=i; 
	for(int i=1;i<=n-m;i++)ans/=i; 
	return ans; 
} 
int32_t main() {
    IOS
    int n,m,k; 
	cin>>n>>m>>k; 
	long long ans=C(n+m-1,m-1); 
	for(int i=1;i<=m;i++) 
	{ 
		if(i&1)ans-=C(m,i)*C(n-(k+1)*i+m-1,m-1); 
		else ans+=C(m,i)*C(n-(k+1)*i+m-1,m-1); 
	} 
	cout<<ans<<endl; 
return 0; 
}