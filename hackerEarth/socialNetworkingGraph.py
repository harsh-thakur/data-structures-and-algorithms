from collections import defaultdict


class Graph:
    def __init__(self, nodes):
        self.V = nodes
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    def bfs(self, s, x):
        q = []
        visited = [False]*self.V
        level = [0]*self.V
        q.append(s)
        visited[s] = True
        level[s] = 0
        while q:
            s = q.pop(0)
            for i in self.graph[s]:
                if visited[i] == False:
                    q.append(i)
                    visited[i] = True
                    level[i] = level[s] + 1

        ans = 0
        for i in level:
            if i == x:
                ans += 1
        return ans


n, e = list(map(int, input().split()))
g = Graph(e+1)
for i in range(e):
    x, y = list(map(int, input().split()))
    g.addEdge(x, y)

q = int(input())
while q > 0:
    q -= 1
    s, k = list(map(int, input().split()))
    print(g.bfs(s, k))
