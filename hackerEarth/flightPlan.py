from collections import defaultdict


class Graph:
    def __init__(self, nodes):
        self.V = nodes
        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    def sortList(self):
        for i in range(self.V):
            self.graph[i].sort()

    def bfs(self, s, d, origin):
        q = []
        q.append(s)
        vis = [False]*1005
        vis[s] = True
        while q:
            s = q.pop(0)
            for i in self.graph[s]:
                if vis[i] == False:
                    origin[i] = s
                    vis[i] = True
                    if i == d:
                        return
                    q.append(i)


n, m, t, c = list(map(int, input().split()))
g = Graph(n+1)
for i in range(n):
    x, y = list(map(int, input().split()))
    g.addEdge(x, y)
g.sortList()
u, v = list(map(int, input().split()))
origin = [-1]*1005
g.bfs(u, v, origin)
# print(origin)
x = origin[v]
ans = []
ans.append(v)
cnt = 1
while x != u:
    ans.append(x)
    x = origin[x]   
    cnt += 1
ans.append(u)
cnt += 1
print(cnt)
j = len(ans) - 1
while j >= 0:
    print(ans[j], end=' ')
    j -= 1
