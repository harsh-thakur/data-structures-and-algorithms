def solve (N, S, A):
    # Write your code here
    R = []
    G = []
    B = []
    A = str(A)
    i = 0
    for c in S:
        if c == 'R':
            R.append(A[i])
        if c == 'G':
            G.append(A[i])   
        if c == 'B':
            B.append(A[i])
        i = i+1
    R.sort()
    G.sort()
    B.sort()
    ans = ""
    i = len(R)-1
    j = len(G)-1
    k = len(B)-1

    for c in S:
        if c == 'R':
            ans = ans + R[i]
            i = i-1
        if c == 'G':
            ans = ans + G[j]
            j = j-1
        if c == 'B':
            ans = ans + B[k]
            k = k - 1   
    return int(ans)

# // RGBR
#   4015    

N = int(input())
S = input()
A = input()

out_ = solve(N, S, A)
print (out_)