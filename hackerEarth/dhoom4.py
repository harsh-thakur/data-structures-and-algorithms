current, target = list(map(int, input().split()))
n = int(input())
a = list(map(int, input().split()))
ans = [-1]*100005
q = []
q.append(current)
ans[current] = 0
while q:
    val = q.pop(0)
    if val == target:
        break
    for i in range(n):
        to = a[i]
        to = to*val
        to = to % 100000
        if ans[to] == -1:
            ans[to] = ans[val] + 1
            q.append(to)
print(ans[target])            