
def leftRotation(a, n):
    ans = []
    i = 1
    while i < n:
        ans.append(a[i])
        i += 1
    ans.append(a[0])
    return ans


def rightRotation(a, n):
    ans = []
    ans.append(a[n-1])
    i = 0
    while i < n-1:
        ans.append(a[i])
        i += 1
    # ans.append(a[0])
    return ans


n = int(input())
a = list(map(int, input().split()))
a.insert(0,0)
q = int(input())
rotation = 0
while q > 0:
    q -= 1
    m = list(map(str, input().split()))
    if m[0] == 'Increment':
        # Increment
        pos = int(m[1]) + (rotation % n)
        pos = pos % n
        a[pos-1] = a[pos-1] + 1
    elif m[0] == 'Left':
        # Rotate Left
        # a = leftRotation(a, n)
        rotation += 1
    elif m[0] == 'Right':
        # a = rightRotation(a, n)
        rotation -= 1
    elif m[0] == 'Update':
        pos = int(m[1]) + (rotation % n)
        pos = pos % n
        val = int(m[2])
        a[pos-1] = val
    else:
        pos = int(m[1]) + (rotation % n)
        pos = pos % n
        # print(a,rotation)
        print(a[pos-1])
