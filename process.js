// let callFun = () => {
// 	console.log('called');
//   setTimeout(()=>{
//   	console.log('set time out called');
//   },0);
//   setImmediate(()=>{
//   	console.log('set immediate');
//   })
//   console.log('executing');
// }
// console.log('initiating function call');
// callFun();
// process.nextTick(()=>{
//   console.log('Process next tick');
// })
// console.log('function call ended');
// let doSomethingElse = ()=> {
// console.log('doing something');
// }
// doSomethingElse();


let racer1 = function() {
  setTimeout(() => console.log("timeout"), 0);
  setImmediate(() => console.log("immediate"));
  process.nextTick(() => console.log("nextTick"));
}

let racer2 = function() {
  process.nextTick(() => console.log("nextTick"));
  setTimeout(() => console.log("timeout"), 0);
  setImmediate(() => console.log("immediate"));
}

let racer3 = function() {
  setImmediate(() => console.log("immediate"));
  process.nextTick(() => console.log("nextTick"));
  setTimeout(() => console.log("timeout"), 0);
}

// racer1()
// racer2()
// racer3()


setTimeout(() => {
  console.log('timeout');
}, 0);

setImmediate(() => {
  console.log('immediate');
});
const fs = require('fs');
const os = require('os')
// console.log(os.arch());
// console.log(os.cpus());
// console.log(os.hostname());
// console.log(os.loadavg());
// console.log(os.userInfo());





// const status = response => {
//   if (response.status >= 200 && response.status < 300) {
//     return Promise.resolve(response)
//   }
//   return Promise.reject(new Error(response.statusText))
// }

// const json = response => response.json()

// fs.stat('seller-details.json', (err,data) => {
//   if(err) {
//     console.log(err)
//   }
// })
  // .then(status)    // note that the `status` function is actually **called** here, and that it **returns a promise***
  // .then(json)      // likewise, the only difference here is that the `json` function here returns a promise that resolves with `data`
  // .then(data => {  // ... which is why `data` shows up here as the first parameter to the anonymous function
  //   console.log('Request succeeded with JSON response', data)
  // })
  // .catch(error => {
  //   console.log('Request failed', error)
  // })


//   const http = require('http')
// // const fs = require('fs')

// const server = http.createServer(function(req, res) {
//   fs.readFile('seller-details.json', 'utf-8', (err, data) => {
//     res.end(data)
//   });
// });
// server.listen(3000, ()=>{
//   console.log('Listening');
// })
// const array = [];
// console.log(array.push('harsh'));
// console.log(array.push('raj'));
// array = []
// const  isEmpty =(obj) => {
//   for (let key in obj) {
//     // if the loop has started, there is a property
//     return false;
//   }
//   return true;
// };

// let a = 6;
// function myFun() {
//   // let a = 7;
//   let x = () => {
//     console.log(a,isEmpty({}));
//   }
//   x();
// }
// myFun();

process.stdin.resume();
process.stdin.setEncoding("utf-8");
var stdin_input = "";

process.stdin.on("data", function (input) {
    stdin_input += input;                               // Reading input from STDIN
});

process.stdin.on("end", function () {
   main(stdin_input);
});

function main(input) {
    console.log("Hi, " + input + ".\n");       // Writing output to STDOUT
}
