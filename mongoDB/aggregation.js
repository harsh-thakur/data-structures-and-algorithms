// mongo "mongodb://cluster0-shard-00-00-jxeqq.mongodb.net:27017,cluster0-shard-00-01-jxeqq.mongodb.net:27017,cluster0-shard-00-02-jxeqq.mongodb.net:27017/aggregations?replicaSet=Cluster0-shard-0" --authenticationDatabase admin --ssl -u m121 -p aggregations --norc


// const pipeline = [
//     {
//         $match:{
//             "imdb.rating": {$gte:7},
//             "genres":{$nin:["Crime","Horror"]},
//             "rated": {$in:["PG", "G"]},
//             languages: { $all: [ "English", "Japanese" ] }
//     }
// ];


// let pipeline = [
//     {
//         $match:{
//             "imdb.rating": {$gte:7},
//             "genres":{$nin:["Crime","Horror"]},
//             "rated": {$in:["PG", "G"]},
//             languages: { $all: [ "English", "Japanese" ] }
//         }
//     },
//     {
//         $project:{
//             _id:0,
//             title:1,
//             rated:1
//         }
//     }
// ]

// let pipeline = [
//     {
//         $project:{
//             _id:0,
//             moveis : {
//                 $split: ["$title"," "]
//             }
//         }
//     }, {
//         $project:{
//             singleWordMovies: {
//                 $eq: [{$size: "$moveis"},1]
//             }
//         }
//     },{$match:{singleWordMovies:true}},{$count:"singleWordMovies"}
// ]


// let pipeline = [
//     {
//         $match: {
//           title: {
//             $type: "string"
//           }
//         }
//       },
//       {
//         $project: {
//           title: { $split: ["$title", " "] },
//           _id: 0
//         }
//       },
//       {
//         $match: {
//           title: { $size: 1 }
//         }
//       }
// ]


// let pipeline = [
//   {
//     $match: {
//       cast : {$elemMatch:{$exists:true}},
//       directors: {$elemMatch:{$exists:true}},
//       writers:{$elemMatch:{$exists:true}},
//     }
//   }, {
//     $project:{
//       _id:0,
//       cast:1,
//       directors:1,
//       writers: {
//         $map: {
//           input: "$writers",
//           as: "writer",
//           in : {
//             $arrayElemAt:[{$split :["$$writer", " ("]},0]
//           }
//         }
//       }
//     }
//   }, 
//   {
//     $project:{
//       "labour of love" : {
//         $gt:[{
//           $size:{$setIntersection:["$cast","$directors","$writers"]}
//         },0]
//       }
//     }
//   },
//   {
//     $match: {
//       "labour of love":true
//     }
//   },{
//     $count:"labour of love"
//   }
// ]


// let pipeline = [
//   {
//     $match: {
//       "tomatoes.viewer.rating": { $gte: 3 },
//       "countries": { $in: ["USA"] },
//       cast: { $elemMatch: { $exists: true } }
//     }
//   },
//   {
//     $project: {
//       _id: 0,
//       rating: "$tomatoes.viewer.rating",
//       title: 1,
//       cast: 1
//     }
//   }, {
//     $addFields: {
//       num_favs: {
//         $size: {
//           $setIntersection: [
//             "$cast", ["Sandra Bullock",
//               "Tom Hanks",
//               "Julia Roberts",
//               "Kevin Spacey",
//               "George Clooney"
//             ]
//           ]
//         }
//       }
//     }
//   }, {
//     $sort: {
//       num_favs: -1,
//       rating: -1,
//       title: -1
//     }
//   }, {$skip:24},{$limit:1}
// ]


// let pipeline = [
//   {
//     $match: {
//       year: { $gte: 1990 },
//       languages: { $in: ["English"] },
//       "imdb.votes": { $gte: 1 },
//       "imdb.rating": { $gte: 1 }
//     }
//   },
//   {
//     $project: {
//       _id: 0,
//       title: 1,
//       "imdb.rating": 1,
//       "imdb.votes": 1,
//       normalized_rating: {
//         $avg: [
//           "$imdb.rating",
//           {
//             $add: [
//               1,
//               {
//                 $multiply: [
//                   9,
//                   {
//                     $divide: [
//                       { $subtract: ["$imdb.votes", 5] },
//                       { $subtract: [1521105, 5] }
//                     ]
//                   }
//                 ]
//               }
//             ]
//           }
//         ]
//       }
//     }
//   },
//   { $sort: { normalized_rating: 1 } },
//   { $limit: 1 }
// ]


// let pipeline = [
//   {
//     $match: {
//       awards : /Won \d{1,2} Oscars?/
//     }
//   }, {
//     $group: {
//       _id:null,
//       highest_rating:{$max:"$imdb.rating"},
//       lowest_rating: {$min: "$imdb.rating"},
//       average_rating: {$avg: "$imdb.rating"},
//       deviation: {$stdDevSamp: "$imdb.rating"}
//     }
//   }
// ]


// let pipeline = [
//   {
//     $match:{
//       languages: {$in:["English"]},
//       cast:{$exists:true}
//     }
    
//   }, {
//     $unwind: "$cast"
//   }, {
//     $group:{
//       _id: "$cast",
//       numFilms:{$sum:1},
//       average_rating: {
//         $avg: "$imdb.rating"
//       }
//     }
//   },
//   {
//     $sort:{numFilms:-1}
//   }
// ]


// db.movies.aggregate([
//   {
//     $match: {
//       languages: "English"
//     }
//   },
//   {
//     $project: { _id: 0, cast: 1, "imdb.rating": 1 }
//   },
//   {
//     $unwind: "$cast"
//   },
//   {
//     $group: {
//       _id: "$cast",
//       numFilms: { $sum: 1 },
//       average: { $avg: "$imdb.rating" }
//     }
//   },
//   {
//     $project: {
//       numFilms: 1,
//       average: {
//         $divide: [{ $trunc: { $multiply: ["$average", 10] } }, 10]
//       }
//     }
//   },
//   {
//     $sort: { numFilms: -1 }
//   },
//   {
//     $limit: 1
//   }
// ])



// db.air_routes.aggregate([
//   {
//     $match: {
//       airplane: /747|380/
//     }
//   },
//   {
//     $lookup: {
//       from: "air_alliances",
//       foreignField: "airlines",
//       localField: "airline.name",
//       as: "alliance"
//     }
//   },
//   {
//     $unwind: "$alliance"
//   },
//   {
//     $group: {
//       _id: "$alliance.name",
//       count: { $sum: 1 }
//     }
//   },
//   {
//     $sort: { count: -1 }
//   }
// ])