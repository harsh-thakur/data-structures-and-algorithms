var pipeline = [
    {
        $match: {
            "languages": {
                $in: ["English"],
                $exists:true
            },
            "imdb.rating": { $gte: 1 },
            "imdb.votes": { $gte: 1 },
            "year": { $gte: 1990 }
        }
    }, {
        $addFields: {
            "normalized_rating": {
                $avg: [
                    "$imdb.rating", {
                        $add: [1, {
                            $multiply: [
                                9, {
                                    $divide: [
                                        {$subtract:["$imdb.votes", 5]},
                                        {$subtract:[ 1521105, 5]}
                                    ]
                                }
                            ]
                        }]
                }]
            }
        }
    },{$sort:{normalized_rating:1}},
    {$project:{
        title:1,
        normalized_rating:1
    }
}
]