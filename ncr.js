function fact(n) {
    let res = 1; 
    for (let i = 2; i <= n; i++) 
        res = res * i; 
    return res; 
}


function nCr(n,r) {
    return fact(n) / (fact(r)*fact(n-r))

}

function solution(n) {
    if(n==1) return 4;
    else 
        return n*4 + nCr(n,2)*32;
}

console.log(solution(4))