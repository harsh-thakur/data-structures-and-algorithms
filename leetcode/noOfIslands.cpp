#include<bits/stdc++.h>
using namespace std;


bool isSafe(vector<vector<char>>&islands,int row,int col,vector<vector<bool>>&visited) {
    int n = islands.size();
    int m = islands[0].size();
    return  (row>=0) && (row<n) && (col>=0) && (col<m) && (islands[row][col] && !visited[row][col]);
}

void dfs(vector<vector<char>>&islands,int row,int col, vector<vector<bool>>&visited) {
    static int rowNbr[] = {-1,0,1,0};
    static int colNbr[] = {0,-1,0,1};
    visited[row][col] = 1;
    for(int i=0;i<4;i++) {
        if(isSafe(islands,row+rowNbr[i],col+colNbr[i],visited)) {
            dfs(islands,row+rowNbr[i],col+colNbr[i],visited);
        }
    }
}

int countNoOfIslands(vector<vector<char>>&islands) {
    int n = islands.size();
    int m = islands[0].size();
    vector<vector<bool>>visited(n);
    for(int i=0;i<n;i++) {
        for(int j=0;j<m;j++) {
            visited[i].push_back(0);
        }
    }

    int noOfIslands = 0;
    for(int i=0;i<n;i++) {
        for(int j=0;j<m;j++) {
            cout<<"visited "<<(visited[i][j])<<" "<<i<<" "<<j<<endl;
            if(islands[i][j] && !visited[i][j]) {
                cout<<"Do dfs\n";
                dfs(islands,i,j,visited);
                noOfIslands++;
            }
        }
    }
    return noOfIslands;
}


int main() {

    int n,m; cin>>n>>m;
    vector<vector<char>>islands(n);
    for(int i=0;i<n;i++) 
        for(int j=0;j<m;j++) {
            int x; cin>>x;
            islands[i].push_back(x);
        }

    cout<<countNoOfIslands(islands);
    return 0;
}