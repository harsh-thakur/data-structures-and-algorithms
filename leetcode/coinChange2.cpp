#include <bits/stdc++.h>
using namespace std;

#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0);
#define endl "\n"
#define int long long
#define f(i, a, b) for (i = a; i < b; i++)
#define rep(i, n) f(i, 0, n)
#define fd(i, a, b) for (i = a; i >= b; i--)
#define pb push_back
#define mp make_pair
#define vi vector<int>

class Solution
{

public:
    int getWays(int amount, int n, vector<int> &coins, int table[])
    {
        cout << amount << " " << n << endl;
        if (amount == 0)
            return 1;
        if (amount < 0)
            return 0;

        if (n <= 0 && amount >= 1)
            return 0;
        if (table[amount] != -1)
            return table[amount];
        table[amount] = getWays(amount, n - 1, coins, table) + getWays(amount - coins[n - 1], n, coins, table);
        return table[amount];
    }
    int change(int amount, vector<int> &coins)
    {
        // if(amount < 1) return 0;
        int table[amount + 1];
        memset(table, -1, sizeof(table));
        table[0] = 1;
        return getWays(amount, coins.size(), coins, table);
    }
};

int32_t main()
{
    Solution s;
    vector<int>coins;
    coins.push_back(1);
    coins.push_back(2);
    coins.push_back(5);
    cout << s.change(5, coins);

    return 0;
}