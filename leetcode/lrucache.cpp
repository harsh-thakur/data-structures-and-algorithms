// LRU Cache

// Solution
// Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.

// get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
// put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.

// The cache is initialized with a positive capacity.

// Follow up:
// Could you do both operations in O(1) time complexity?


#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >

class LRUCache {
public:

    struct Node {
        pair<int,int> data;
        Node* next;
        Node* prev;
        Node(pair<int,int> val) : data(val), next(nullptr), prev(nullptr) {} 
    };
    Node* head;
    Node* tail;
    map<int, Node*>cache;
    int capacity;
    int noOfNodes;
    LRUCache(int capacity): head(nullptr), tail(nullptr){
        this->capacity = capacity;
        this->noOfNodes = 0;
    }
    
    int get(int key) {
        // cout<<"Key==="<<key<<" "<<cache[key]<<endl;
        if(cache[key]) {
            Node* node = cache[key];
            // cout<<node->data.first<<endl;
            int value = node->data.second;
            detachNode(node);
            insertFront(key,value);
            return value;
        }
        return -1;
    }
//     ["LRUCache","put","put","get","put","get","put","get","get","get"]
//     [[2],[1,1],[2,2],[1],[3,3],[2],[4,4],[1],[3],[4]]

// ["LRUCache","get","put","get","put","put","get","get"]
// [[2],[2],[2,6],[1],[1,5],[1,2],[1],[2]]

    // null<-1 <-> 3<-> 4->null

    void detachNode(Node * node) {
            Node* currentNode = node;
            Node* previous = currentNode->prev;
            if(noOfNodes==1) {
                head = nullptr;
                tail = nullptr;
                 delete currentNode;
                --noOfNodes; return;
            }
            if(previous==nullptr) {
                head = head->next;
                head->prev = nullptr;
            }else if(currentNode->next == nullptr){
                previous->next = nullptr;
                // cout<<"Else";
                // previous->next = currentNode->next;
                // previous->next->prev = previous;
                // delete currentNode;
                tail = previous;
            } else {
                previous->next = currentNode->next;
                previous->next->prev = previous;
            }
            currentNode->next = nullptr;
            currentNode->prev = nullptr;
            delete currentNode;
            --noOfNodes;
    }

    void insertFront(int key, int val) {
        
        Node *node = new Node({key,val});
        Node * temp = head;
        if(head == nullptr) {
            head = node;
            tail = node;
            // cout<<head<<endl;
            cache[key] = head;
            // cout<<cache[key];
        }else {
            node->next = head;
            head = node;
            node->next->prev = node;
            cache[key] = head;
        }
        ++noOfNodes;
    }
    void removeLastNode() {
        Node * node = tail;
        int key = tail->data.first;
        if(noOfNodes==1) {
                head = nullptr;
                tail = nullptr;
                cache.erase(key);
                 delete node;
                --noOfNodes; return;
        }
        // cout<<key<<node->prev->data.second<<endl;
        cache.erase(key);
        tail = node->prev;
        tail->next = nullptr;
        --noOfNodes;
    }

    void replace(int key,int value) {
        Node* node = cache[key];
        detachNode(node);
        insertFront(key,value);
    }
    
    void put(int key, int value) {
        // cout<<noOfNodes<<" "<<key<<endl;
        if(cache[key]) {
            replace(key,value);
        } else if(noOfNodes == capacity) {
            removeLastNode();
            insertFront(key,value);
        } else insertFront(key,value);
    }
};


int32_t main() {
    IOS
    int capacity; cin>>capacity;
    LRUCache* obj = new LRUCache(capacity);
    int q; cin>>q;
    while (q--) {
        /* code */
        int operationCode; cin>>operationCode;
        if(operationCode==0){
            int key; cin>>key;
            int param_1 = obj->get(key);
            cout<<param_1<<endl;
        }
        if(operationCode==1){
            int key, value; cin>>key>>value;
            obj->put(key,value);
        }
        
    }
    
    return 0;
}