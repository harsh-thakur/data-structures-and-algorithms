// Given two sorted arrays, merge elements such that they are sorted and their size remains the same.
// Time: O(mn)
// Space: O(1)
// input : [1,4,7,8,10] [2,3,9]
// output: [1,2,3,4,7] [8,9,10]


#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int x,y; cin>>x>>y;
        vector<int>v1(x),v2(y);
        for(int i=0;i<x;i++) cin>>v1[i];
        for(int i=0;i<y;i++) cin>>v2[i];

        int i=0,j=0;
        while(i<x && j<y) {
            if(v1[i] > v2[j]) {
                int t = v2[j];
                v2[j] = v1[i];
                v1[i] = t;
                sort(v2.begin(),v2.end());
            } 
             i++;      
        }
        for(i=0;i<x;i++) cout<<v1[i]<<" ";
        for(i=0;i<y;i++) cout<<v2[i]<<" ";
        cout<<endl;
    }
    
    return 0;
}