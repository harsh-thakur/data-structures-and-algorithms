//   Bitwise AND of Numbers Range

// Solution
// Given a range [m, n] where 0 <= m <= n <= 2147483647, return the bitwise AND of all numbers in this range, inclusive.

// Example 1:

// Input: [5,7]
// Output: 4
// Example 2:

// Input: [0,1]
// Output: 0

#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


class Solution {
public:
    int setBitNumber(int n) { 
    if (n == 0) 
        return 0; 
    int msb = 0; 
    while (n != 0) { 
        n = n >> 1; 
        msb++; 
    } 
    return (1 << (msb-1)); 
}

    //  1111
    // 10000
    // 10001
    // 5 7

    int rangeBitwiseAnd(int m, int n) {
        if(n==2147483647 && m== 2147483647) return n;
        // if(!(n & n-1)) return 0;
        int ans = m;
        // int i=setBitNumber(n);
        // cout<<ans;
        long long i = m+1;
        
        for(;i<=n;++i) {
            ans &= i;
            if(ans == 0) return 0;
        }
        return ans;
    }

    void solve() {
        int n,m; cin>>n>>m;
        cout<<rangeBitwiseAnd(n,m)<<endl;
    }
};

int32_t main() {
    IOS
    int t; cin>>t;
    Solution s;
    while (t--) {
        /* code */
        s.solve();
        
        
    }
    
    return 0;
}