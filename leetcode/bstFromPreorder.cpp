#include <bits/stdc++.h>
using namespace std;

struct Node
{
    int val;
    Node *left;
    Node *right;
    Node() : val(0), left(NULL), right(NULL) {}
    Node(int x) : val(x), left(NULL), right(NULL) {}
    Node(int x, Node *left, Node *right) : val(x), left(left), right(right) {}
};

class Solution
{
public:
    Node *constructBSTfromPreorderTraversal(vector<int> &P, int *currentIndex, int low, int high)
    {
        if (*currentIndex >= P.size() || low > high)
            return NULL;
        Node *root = new Node(P[*currentIndex]);
        *currentIndex += 1;
        if (low == high)
            return root;
        int i;
        for (i = low; i <= high; ++i)
        {
            if (P[i] > root->val)
                break;
        }
        root->left = constructBSTfromPreorderTraversal(P, currentIndex, *currentIndex, i - 1);
        root->right = constructBSTfromPreorderTraversal(P, currentIndex, i, high);
        return root;
    }
    Node *solve(vector<int> &P)
    {
        int currentIndex = 0;
        return constructBSTfromPreorderTraversal(P, &currentIndex, 0, P.size() - 1);
    }
    void inorderTraversal(Node *root)
    {
        if (!root)
        {
            return;
        }
        inorderTraversal(root->left);
        cout << root->val << " ";
        inorderTraversal(root->right);
    }
};

int main()
{
    int n;
    cin >> n;
    vector<int> P(n);
    for (int i = 0; i < n; i++)
        cin >> P[i];
    Node *root = NULL;
    Solution s;
    root = s.solve(P);
    s.inorderTraversal(root);
    return 0;
}