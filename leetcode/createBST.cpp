#include<bits/stdc++.h>
using namespace std;

class BST {
    int data; 
    BST *left, *right;
    public:
    BST();
    BST(int);

    BST* insert(BST *, int);
    void inorderTraversal(BST*);
};

BST::BST(): data(0), left(NULL), right(NULL) {}
BST::BST(int value): data(value), left(NULL), right(NULL) {}

// Insert Function

BST*BST :: insert(BST* root, int value) {
    if(!root) {
        return new BST(value);
    }
    if(value > root->data) {
        root->right = insert(root->right,value);
    } else {
        root->left = insert(root->left,value);
    }
    return root;
}


// Inorder traversal
void BST :: inorderTraversal(BST*root) {
    if(!root) {
        return;
    }
    inorderTraversal(root->left);
    cout<<root->data<<" ";
    inorderTraversal(root->right);
}

int main() {
    BST b, *root = NULL;
    int n; cin>>n;
    // vector<int>v(n);
    for(int i=0;i<n;i++) {
        int x; cin>>x;
        root = b.insert(root,x);
    }
    b.inorderTraversal(root);

    return 0;
}