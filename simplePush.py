t = int(input())

while t > 0:
    t -= 1
    n = int(input())
    a = list(map(int, input().split()))
    b = list(map(int, input().split()))
    ans = "Yes"
    x = [0]*(n+2)
    for i in range(n):
        x[i+1] = b[i] - a[i]
    x[0] = 0
    x[n+1] = 0

    k = 0
    for i in range(n+1):
        if x[i] != x[i+1]:
            k += 1
        if x[i] < 0:
            k = 10    

    if k > 2:
        ans = "NO"
    print(ans)
