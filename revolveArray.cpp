#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007

int n,q;


void build(vector<long long>&T,vector<long long>&v,int node,int left,int right){
    if(left==right){
        T[node] = v[left]%mod;
    }else{
        int mid = left + right >> 1;
        build(T,v,2*node,left,mid);
        build(T,v,2*node+1,mid + 1,right);
        
        T[node] = (T[2*node]%mod + T[2*node + 1]%mod)%mod;
    }
}
void update(vector<long long>&T,vector<long long>&L,int node,int &val,int left,int right,int l,int r){
    if(L[node]!=0){
        // cout<<L[node]<<endl;
        T[node] += L[node]%mod; 
        if(left!=right){
            int totalNodes = right-left + 1;
            int a = (2*L[node]/totalNodes + totalNodes - 1)/2;
            int leftNodes = (left+right >> 1)-left+1;
            int leftLazy = leftNodes*(2*a-leftNodes + 1)/2;
            int rightLazy = L[node] - leftLazy;
            L[2*node] += leftLazy;
            L[2*node + 1] += rightLazy;
        }
        L[node] = 0;
    }
    if(left>right || left>r || right<l) return;
    if(left>=l && right<=r){
        int totalNodes = (right-left+1);
        int tempVal = (totalNodes*(2*val - totalNodes - 1))/2;
        T[node] = (T[node]%mod + tempVal%mod)%mod;
         if(left!=right){
            int noOfleftNodes = (left+right >> 1)-left+1;
            int noOfRightNodes = right - ((left+right >> 1) + 1) + 1;
            int lazyLeft = (noOfleftNodes*(2*val - noOfleftNodes + 1))/2;
            int valR = val-((left+right >> 1)-left+1);
            int lazyRight = (noOfRightNodes*(2*valR - noOfRightNodes + 1))/2;
            L[2*node] += lazyLeft;
            L[2*node + 1] += lazyRight;
             val = valR;
        }
        return;
    }
    int mid = left + right >> 1;
    update(T,L,2*node,val,left,mid,l,r);
    update(T,L,2*node + 1,val,mid+1,right,l,r);
    
    T[node] = (T[2*node]%mod + T[2*node+1]%mod)%mod;
}

long long query(vector<long long>&T,vector<long long>&L,int node,int left,int right,int l,int r){
    if(L[node]!=0){
        T[node] += L[node];
        if(left!=right){
            int totalNodes = right-left + 1;
            int a = (2*L[node]/totalNodes + totalNodes - 1)/2;
            int leftNodes = (left+right >> 1)-left+1;
            int leftLazy = leftNodes*(2*a-leftNodes + 1)/2;
            int rightLazy = L[node] - leftLazy;
            L[2*node] += leftLazy;
            L[2*node + 1] += rightLazy;
        }
        L[node] = 0;
    }
    if(left>right || left>r || right<l) return 0;
    if(left>=l && right<=r){
        return T[node]%mod;
    }
    int mid = left + right >> 1;
    return (query(T,L,2*node,left,mid,l,r)%mod + 
    query(T,L,2*node + 1,mid+1,right,l,r)%mod)%mod;
   
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    cin>>n>>q;
    int i;
    vector<long long>v(n);
    for(i=0;i<n;i++) cin>>v[i];
    
    int t = (int)ceil(log2(n));
    int max_size = 2*(int)pow(2,t);
    
    vector<long long>T(max_size,0);
    vector<long long>L(max_size,0);
    
    build(T,v,1,0,n-1);

    int type,x,y;
    
    while(q--){
        cin>>type>>x>>y;
        if(type==1){
            //update
            int no = n - x -1 ;
            if(no<=y){
                update(T,L,1,y,0,n-1,x-1,n-1);
                int remainingVal = y - no;
                if(remainingVal!=0)
                update(T,L,1,remainingVal,0,n-1,0,remainingVal-1);
            }
            else{
                update(T,L,1,y,0,n-1,x-1,y-1);
            }
            
            // for(i=0;i<max_size;i++){
            //     cout<<T[i]<<" ";
            // }
            // cout<<endl;
            // for(i=0;i<max_size;i++){
            //     cout<<L[i]<<" ";
            // }
            
        }else{
            //query
            if(x>y){
                long long first = query(T,L,1,0,n-1,x-1,n-1);
                long long second = query(T,L,1,0,n-1,0,y-1);
                cout<<(first+second)%mod<<endl;
            }else{
                cout<<query(T,L,1,0,n-1,x-1,y-1)<<endl;
            }
            
        }
    }
    return 0;
}