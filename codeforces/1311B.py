te = int(input())
while te > 0:
    te -= 1
    x, y = list(map(int, input().split()))
    numbers = list(map(int, input().split()))
    positions = list(map(int, input().split()))
    pos = [0]*x
    for i in positions:
        pos[i-1] = 1
    # print(pos)
    # while True:
    #     ok = False
    #     for j in range(x-1):
    #         if pos[j] == 1 and numbers[j] > numbers[j+1]:
    #             ok = True
    #             a = numbers[j]
    #             numbers[j] = numbers[j+1]
    #             numbers[j+1] = a
    #     if not ok:
    #         break

    numbers.sort()
    # print(numbers)
    ok = True
    for i in range(x-1):
        ok &= numbers[i] <= numbers[i+1]
    if ok:
        print('Yes')
    else:
        print('No')
