#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    IOS
    int n,p,q,r; cin>>n>>p>>q>>r;
    vector<int>v(n);
    for(int i=0;i<n;i++) cin>>v[i];
    int minLeft[n], maxLeft[n],minRight[n],maxRight[n];
    minLeft[0] = v[0];
    for(int i=1;i<n;i++) minLeft[i] = min(minLeft[i-1],v[i]);
    maxLeft[0] = v[0];
    for(int i=1;i<n;i++) maxLeft[i] = max(maxLeft[i-1],v[i]);
    minRight[n-1] = v[n-1];
    for(int i=n-2;i>=0;--i) minRight[i] = min(minRight[i+1],v[i]);
    maxRight[n-1] = v[n-1];
    for(int i=n-2;i>=0;--i) maxRight[i] = max(maxRight[i+1],v[i]);
    int sum = v[0]*q;
    sum += p>0 ? maxLeft[0]*p : minLeft[0]*p;
    sum += r>0 ? maxRight[0]*r : minRight[0]*r;
    int x = sum;
    for(int i=1;i<n;++i) {
        sum = v[i]*q;
        sum += p>0 ? maxLeft[i]*p : minLeft[i]*p;
        sum += r>0 ? maxRight[i]*r : minRight[i]*r;
        x = max(sum,x);
    }
    cout<<x;
    return 0;
}

// -648105427274517138 314439466956710001 -42740706758433429 -420732742271772282 319934024300681443
// -60186617891961174 29200570233551523 -3969136004492367 -39071545659225486 29710824907297689
// 49939212226556298 -24228865570207821 3293348791258209 32419203454797522 -24652244011035703
