te = int(input())
while te > 0:
    te -= 1
    n, k = list(map(int, input().split()))
    count = 1
    i = n-1
    j = n
    while count < k:
        i -= 1
        count += 1
        while count < k:
            j -= 1
            count += 1
            if k - count >= j - i - 1:
                # print(j,i)
                count += j-i-1
                j = i+1
            if i+1 == j and i > 1 and count != k:
                # print(i,j,count)
                i -= 1
                j = n
                count += 1
    ans = ''
    # print(i, j)
    for s in range(n):
        if s+1 == i or s+1 == j:
            ans += 'b'
        else:
            ans += 'a'
    print(ans)
