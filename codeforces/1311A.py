# 5
# 2 3
# 10 10
# 2 4
# 7 4
# 9 3

# 1
# 0
# 2
# 2
# 1


te = int(input())
while te > 0:
    te = te - 1
    a, b = list(map(int, input().split()))
    ans = 0
    if a == b:
        ans = 0
    elif a > b:
        if (a - b) % 2 == 0:
            ans = 1
        else:
            ans = 2
    else:
        if (b-a) % 2 == 0:
            ans = 2
        else:
            ans = 1
    print(ans)                             