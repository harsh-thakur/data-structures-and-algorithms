te = int(input())
while te > 0:
    te = te - 1
    n, s, q = list(map(int, input().split()))
    closedFloors = list(map(int, input().split()))
    floors = {}
    for i in closedFloors:
        floors[i] = 0
    # print(floors)
    f = s
    b = s
    ans = 0
    while f > 0 and b <= n:
        if floors.get(f) is None or floors.get(b) is None:
            break
        ans += 1
        f -= 1
        b += 1
    if f <= 0:
        while b <= n:
            if floors.get(b) is None:
                break
            ans += 1
            b += 1
    if b > n:
        while f > 0:
            if floors.get(f) is None:
                break
            ans += 1
            f -= 1
    print(ans)
