MAX = 100009
id = []


def initialize():
    for i in range(MAX):
        id.append(i)


def root(x):
    while id[x] != x:
        x = id[x]
    return x


def Union1(x, y):
    root_x = root(x)
    root_y = root(y)
    id[root_x] = root_y


def find1(x, y):
    if root(x) == root(y):
        return True
    return False


def kruskal(n, edge_graph):
    minimumCost = 0
#     print(edge_graph)
    for i in range(len(edge_graph)):
        x = edge_graph[i][1]
        y = edge_graph[i][2]
        cost = edge_graph[i][0]
        if root(x) != root(y):
            minimumCost += cost
            Union1(x, y)
    return minimumCost


nNodes, nEdges = map(int, input().split())
edges_1 = []
initialize()
Dict = {}
for i in range(nEdges):
    u, v = list(map(int, input().split()))
    edges_1.append((u, v))
    Dict[str(u)+str(v)] = 1
#     Union1(u, v)
# print(Dict)
edge_graph = []
for i in range(1, nNodes+1):
    for j in range(i+1, nNodes+1):
        if not Dict.get(str(i)+str(j)) and not Dict.get(str(j)+str(i)):
            edge_graph.append((0, i, j))

for i in edges_1:
    edge_graph.append((1, i[0], i[1]))

minimumCost = kruskal(nNodes, edge_graph)
print(minimumCost)
