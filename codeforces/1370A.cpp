#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long


int findMaxGCD(vector<int>arr, int n) { 
    // Computing highest element 
    int high = n/2;
    // Array to store the count of divisors 
    // i.e. Potential GCDs 
    int ans = INT_MIN;
    for(int i=high; i>=1; i--) {
        // cout<<i<<endl;
        int t = n/i;
        // cout<<t<<endl;
        if(t > 1 && i > ans) {ans = i; break;} 
    }
    return ans;
} 

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>v(n);
        for(int i=0;i<n; i++) v[i] = i+1;

        cout << findMaxGCD(v, n)<<endl;;
    }
    return 0;
}