import math


def primeFactors(n):
    prime = []
    while n % 2 == 0:
        prime.append(2)
        n = n // 2
    i = 3
    while i <= math.sqrt(n):
        while n % i == 0:
            prime.append(i)
            n = n//i
        i += 2
    if n > 2:
        prime.append(n)
    return prime


te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    factors = primeFactors(n)
    # print(factors)
    if len(factors) < 3:
        print('NO')
    else:
        if len(factors) == 3:
            x, y, z = factors
        else:
            x = factors[0]
            j = 3
            if factors[0] != factors[1]:
                y = factors[1]
                j = 2
            else:
                y = factors[1]*factors[2]
            z = 1
            while j < len(factors):
                z = z*factors[j]
                j += 1
        if x == y or y == z or x == z:
            print('NO')
        else:
            print('YES')
            print(x, y, z)
