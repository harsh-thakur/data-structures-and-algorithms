def isPrime(n):
    if (n <= 1) : 
        return False
    if (n <= 3) : 
        return True
  
    # This is checked so that we can skip  
    # middle five numbers in below loop 
    if (n % 2 == 0 or n % 3 == 0) : 
        return False
  
    i = 5
    while(i * i <= n) : 
        if (n % i == 0 or n % (i + 2) == 0) : 
            return False
        i = i + 6
  
    return True


n = int(input())
if isPrime(n) or n == 1:
    print(n)
else:
    p = 2
    x = True
    while x:
        if n % p == 0:
            x = False
        else:
            p += 1
    q = 0
    while n > 1:
        if n % p != 0:
            q = n
            break
        n = int(n / p)
    if q != 0:
        print(1)
    else:
        print(p)       
        

