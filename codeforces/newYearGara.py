te = int(input())
while te > 0:
    te -= 1
    c = list(map(int, input().split()))
    c.sort()
    if c[2] > c[0] + c[1] + 1:
        print('No')
    else:
        print('Yes')
