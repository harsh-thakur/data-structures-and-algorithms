#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        string s; cin>>s;
        string ans = "";
        int len = s.size();
        ans += s[0];
        for(int i=1;i<len-1;i+=2) ans+=s[i];
        ans += s[len-1];
        cout<<ans<<endl;
    }
    
    return 0;
}