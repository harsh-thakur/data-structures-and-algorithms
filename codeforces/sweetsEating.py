n, m = map(int, input().split())
sugar = list(map(int, input().split()))
sugar.sort()
cummlativeSugarLevel = []
cSum = 0
ans = []
for i in range(m):
    cummlativeSugarLevel.append(0)
    ans.append(0)
for i in sugar:
    cSum += i
    cummlativeSugarLevel.append(cSum)

# print(cummlativeSugarLevel)
k = 0
for i in range(n):
    k += 1
    j = k + m - 1
    x = cummlativeSugarLevel[j] + ans[j-m]
    ans.append(x)
for i in range(m, n+m):
    print(ans[i], end=" ")
