n = int(input())
for i in range(n):
    nPlanks = int(input())
    planks = list(map(int, input().split()))
    planks.sort()
    ans = planks[nPlanks-1]
    x = True
    while x:
        if ans <= nPlanks and planks[nPlanks - ans] >= ans:
            x = False
        else:
            ans -= 1
    print(ans)
