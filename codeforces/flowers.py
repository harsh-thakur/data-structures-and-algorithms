MOD = 1000000007
dp = [-1]*100007


def solution(n, k):
    if n < 0:
        return 0
    if n == 0 or n == 1:
        return 1
    if dp[n] != -1:
        return dp[n] % MOD
    dp[n] = (solution(n-1, k) % MOD + solution(n-k, k) % MOD) % MOD
    return dp[n] % MOD


t, k = map(int, input().split())
while t > 0:
    t -= 1
    a, b = map(int, input().split())
    ans = 0
    for i in range(a, b+1):
        # print(i)
        ans = (ans % MOD + solution(i, k) % MOD) % MOD
    print(ans % MOD)
