n = int(input())
events = list(map(int, input().split()))
sumTillNow = 0
eventsState = [-1]*10000007
currentEvents = []
d = 0
ansEvents = []
i = 0
notPossible = 0
while i < n:
    if events[i] < 0 and eventsState[abs(events[i])] == -1:
        # print('not possible')
        notPossible = 1
        break
    elif events[i] > 0 and eventsState[events[i]] != 0 and eventsState[events[i]] != 1:
        # print('positive')
        sumTillNow += events[i]
        eventsState[abs(events[i])] = 0
        currentEvents.append(events[i])
    elif events[i] < 0 and eventsState[abs(events[i])] == 0:
        # print('negative')
        sumTillNow += events[i]
        eventsState[abs(events[i])] = 1
        if sumTillNow == 0:
            # print('sum 0')
            d += 1
            ansEvents.append(len(currentEvents)*2)
            for j in range(len(currentEvents)):
                eventsState[currentEvents[j]] = -1
            currentEvents.clear()
    else:
        # print('else')
        notPossible = 1
        break
    i += 1
if notPossible == 1 or d == 0 or sumTillNow != 0:
    print(-1)
else:
    print(d)
    for i in ansEvents:
        print(i, end=' ')
