te = int(input())
while te > 0:
    te -= 1
    a, b, p = list(map(int, input().split()))
    crossRoads = input()
    dp = list()
    dp.append((1, 0))
    currentSum = 0
    i = 1
    l = len(crossRoads)
    while i < l:
        if crossRoads[i] != crossRoads[i-1]:
            if crossRoads[i] == 'B':
                currentSum += a
            else:
                currentSum += b
            dp.append((i+1, currentSum))
        i += 1
    if crossRoads[l-1] == crossRoads[l-2]:
        if crossRoads[l-1] == 'A':
            currentSum += a
        else:
            currentSum += b
        dp.append((l, currentSum))
    dl = len(dp)
    i = 0
    ans = 1
    while i < dl:
        if dp[dl-1][1] - dp[i][1] <= p:
            ans = dp[i][0]
            break
        i += 1
    print(ans)
