from collections import defaultdict


class G:
    def __init__(self, vertices):
        self.graph = defaultdict(list)
        self.V = vertices

    def addPath(self, u, v):
        self.graph[u].append(v)
        self.graph[v].append(u)

    def bfs(self, s, d, visited, pathList, values):

        # print('length', self.V)
        q = []
        q.append(s)
        pathList.append(s)
        visited[s] = True
        while q:
            s = q.pop()
            if s == d:
                pathList.append(d)
                print(pathList)
                return
            for i in self.graph[s]:
                # print(i)
                if i == d:
                    pathList.append(d)
                    m = {}
                    for i in values:
                        m[i] = 0
                    # print(m,pathList)
                    for i in pathList:
                        m[values[i-1]] += 1
                    # m.sort()
                    maxCount = -1
                    ans = ''
                    for i in m:
                        if maxCount < m[i]:
                            maxCount = m[i]
                            ans = i
                    print(ans)
                    return
                if visited[i] == False:
                    # q.append(i)
                    self.bfs(i, d, visited, pathList, values)
                    # visited[i] = True
        pathList.pop()
        return

    def path(self, s, d, values):
        visited = [False]*(self.V)
        pathList = []
        self.bfs(s, d, visited, pathList, values)


n = int(input())
g = G(n+1)
values = list(map(str, input().split()))
for i in range(n-1):
    x, y = list(map(int, input().split()))
    g.addPath(x, y)
q = int(input())
for i in range(q):
    x, y = list(map(int, input().split()))
    g.path(x, y, values)
