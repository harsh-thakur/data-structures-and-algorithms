te = int(input())
while te > 0:
    te -= 1
    A, B, x, y = list(map(int, input().split()))
    ans = max(A*y, max(x*B, max((A-1-x)*B, A*(B-1-y))))
    print(ans)
