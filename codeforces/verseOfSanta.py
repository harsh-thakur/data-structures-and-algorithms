def findMax(v, n):
    i = 0
    ans = -1
    ansIndex = 0
    while i <= n:
        if ans < v[i]:
            ans = v[i]
            ansIndex = i
        i += 1
    return ansIndex


te = int(input())
while te > 0:
    te -= 1
    n, s = list(map(int, input().split()))
    v = list(map(int, input().split()))
    cSum = []
    cSum.append(v[0])
    for i in range(1, n):
        cSum.append(cSum[i-1]+v[i])
    index = 0
    for i in range(n):
        if s == cSum[i]:
            index = i
            break
        if s < cSum[i]:
            index = i-1
            break
    if index < 0:
        print(0)
    else:
        m = findMax(v, index)
        if index + 2 <= n-1 and v[m] >= v[index+1] + v[index+2]:
            print(m+1)
        else:
            print(0)
