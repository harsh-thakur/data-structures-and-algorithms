t = int(input())
for i in range(t):
    n = int(input())
    s = list(input())
    t = list(input())
    diff = []
    for j in range(n):
        if s[j] != t[j]:
            diff.append(j)

    if len(diff) > 2 or len(diff) < 2:
        print('No')
    elif s[diff[0]] == s[diff[1]] and t[diff[0]] == t[diff[1]]:
        print('Yes')
    else:
        print('No')    