#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int> even, odd;
        for(int i=1;i<=2*n;i++) {
            int x; cin>>x;
            if(x & 1) odd.push_back(i);
            else even.push_back(i);
        }
        vector<pair <int,int> >ans;
        for(int i=0; i+1<odd.size(); i += 2) {
            ans.push_back({odd[i], odd[i+1]});
        }
         for(int i=0; i+1<even.size(); i += 2) {
            ans.push_back({even[i], even[i+1]});
        }
        for(int i=0;i<n-1; i++) {
            cout<<ans[i].first<<" "<<ans[i].second<<endl;
        }

    }
    
    return 0;
}