te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    B = list(map(int, input().split()))
    D = {}
    for i in B:
        D[i] = 1
    ans = []
    i = 0
    while i < n:
        ans.append(B[i])
        k = B[i]
        while k < 2*n:
            k += 1
            if D.get(k) is None:
                D[k] = 1
                ans.append(k)
                break
        i += 1
    if len(ans) == 2*n:
        for i in ans:
            print(i, end=" ")
    else:
        print(-1)
    # print(D)
