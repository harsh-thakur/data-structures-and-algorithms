te = int(input())
while te > 0:
    te -= 1
    n, x = list(map(int, input().split()))
    num = list(map(int, input().split()))
    m = {}
    for i in num:
        if m.get(i) is None:
            m[i] = 1
        else:
            pass
    i = 0
    current = 1
    while x > 0:
        if m.get(current) is None:
            m[current] = 1
            x -= 1
            if m.get(current+1):
                current += 1
        elif m.get(current) is None and x < 1:
            break
        elif m[current] == 1:
            # print('here')
            current += 1
    print(current)
