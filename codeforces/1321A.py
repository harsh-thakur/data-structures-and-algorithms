te = int(input())
robo = list(map(int, input().split()))
bionic = list(map(int, input().split()))
r = 0
b = 0
c = 0
f = 0
for i in range(te):
    if robo[i] == 1 and bionic[i] == 1:
        r += 1
        b += 1
        c += 1
    elif robo[i] == 1:
        f = 1
        r += 1
    elif bionic[i] == 1:
        f = 1
        b += 1
# print(r,b,c,f)     
if r == b and c == 0 and f == 0 or c == te or (r==b and r == c and f == 0):
    print(-1)
else:
    # print(r,b,c)
    q = b - c + 1
    p = r - c
    if q % p == 0:
        print(q//p)
    else:
        print((q//p)+1)