te = int(input())
while te > 0:
    te -= 1
    num = list(input())
    n = len(num)
    evens = []
    odds = []
    for i in num:
        if int(i) & 1 == 1:
            odds.append(i)
        else:
            evens.append(i)
    ans = ''
    i = 0
    j = 0
    while i < len(evens) and j < len(odds):
        if int(evens[i]) < int(odds[j]):
            ans += evens[i]
            i += 1
        else:
            ans += odds[j]
            j += 1

    if i == len(evens):
        while j < len(odds):
            ans += odds[j]
            j += 1            
    elif j == len(odds):
        while i < len(evens):
            ans += evens[i]
            i += 1
    print(ans)                    
# 153666640917
