n, x = list(map(int, input().split()))
mods = [0]*x
vals = []
for i in range(x):
    vals.append((mods[i], i))
# print(vals)
mn = -1 
cnt = -1
while n > 0:
    n -= 1
    cur = int(input())
    cur = cur % x
    vals.remove((mods[cur], cur))
    mods[cur] += 1
    vals.append((mods[cur], cur))
    # sorted_x = vals.sort()
    if cnt < mods[cur]:
        cnt = mods[cur]
        mn = cur
    # else:
    #     cnt = vals[0][0]
    #     mn = vals[0][1]
    ans = cnt*x + mn
    print(ans)
