# 2147483647
n = int(input())
numbers = list(map(int, input().split()))
candidate = 0
prefix = [0]*n
suffix = [0]*n
prefix[0] = suffix[n-1] = 2147483647
for i in range(1, n):
    prefix[i] = prefix[i-1] & (~numbers[i-1])
i = n-2
while i >= 0:
    suffix[i] = suffix[i+1] & (~numbers[i+1])
    i -= 1
ans = -1
pos = -1
for i in range(n):
    tmp = prefix[i] & numbers[i] & suffix[i]
    if tmp > ans:
        ans = tmp
        pos = i
print(numbers[pos], end=" ")
for i in range(n):
    if i != pos:
        print(numbers[i], end=" ")
