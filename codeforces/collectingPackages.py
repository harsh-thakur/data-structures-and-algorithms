te = int(input())
while te > 0:
    te = te - 1
    n = int(input())
    listOfCoordinates = []
    for _ in range(n):
        a, b = list(map(int, input().split()))
        listOfCoordinates.append((a, b))
    listOfCoordinates.sort()
    tup = sorted(listOfCoordinates, key=lambda x: x[1])
    isPossible = 1
    ans = ''
    x = 0
    y = 0
    # print(tup)
    for i in range(n):
        diff_x = tup[i][0] - x
        diff_y = tup[i][1] - y

        if diff_x < 0 or diff_y < 0:
            isPossible = 0
            break
        else:
            while diff_x > 0:
                diff_x -= 1
                ans += 'R'
            while diff_y > 0:
                diff_y -= 1
                ans += 'U'
        x = tup[i][0]
        y = tup[i][1]        
    if isPossible == 0:
        print('NO')
    else:
        print('YES')
        print(ans)
