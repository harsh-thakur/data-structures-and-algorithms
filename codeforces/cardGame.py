te = int(input())
while te > 0:
    te -= 1
    n, k1, k2 = list(map(int, input().split()))
    p1 = list(map(int, input().split()))
    p2 = list(map(int, input().split()))
    p1.sort()
    p2.sort()
    if p1[k1-1] > p2[k2-1]:
        print('YES')
    else:
        print('NO')
