te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    p = list(map(int, input().split()))
    Xor = 0
    S = 0
    for i in p:
        S += i
        Xor ^= i
    print(2)
    print(Xor, S + Xor)