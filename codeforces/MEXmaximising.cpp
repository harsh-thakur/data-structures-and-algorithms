#include<bits/stdc++.h>
using namespace std;

int main() {
	// your code goes here
	int q,x; cin>>q>>x;
	vector<int>mods(x);
	set<pair<int, int>>vals;
	for(int i=0;i<x;i++) vals.insert(make_pair(mods[i],i));
	while(q--) {
	    long c; cin>>c;
	    c %= x;
	    vals.erase(make_pair(mods[c],c));
	    ++mods[c];
	    vals.insert(make_pair(mods[c],c));
	    cout<<vals.begin()->first*x + vals.begin()->second<<endl;
	}
	return 0;
}
