te = int(input())
while te > 0:
    te -= 1
    a, b, c, n = list(map(int, input().split()))
    totalCoins = a + b + c + n
    m = -1
    m = max(a, max(b, c))
    equalizedCoins = m - a + m - b + m - c
    if n - equalizedCoins < 0:
        print('NO')
    elif (n - equalizedCoins) % 3 == 0:
        print('YES')
    else:
        print('NO')


# 1 1 5 3
# 1 1 5 4
# 1 1 5 5
# 1 1 5 6
# 1 1 5 7
# 1 1 5 8
# 1 1 5 9
# 1 1 5 10
# 1 1 6 1
# 1 1 6 2
# 1 1 6 3
# 1 1 6 4
