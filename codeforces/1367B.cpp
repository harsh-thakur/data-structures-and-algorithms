#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>v(n);
        for(int i=0;i<n; i++) cin>>v[i];
        int moves = 0;
        // 3 2 7 6
        for(int i=0;i<n;i++) {
            if(i%2==1 && v[i]%2==0) {
                int j = i+1;
                // search the first odd at even place;
                while(j<n) {
                    if(v[j] %2==0) {
                        j+=2;
                    }else {
                        int t = v[j];
                        v[j] = v[i];
                        v[i] = t;
                        moves++;
                        break;
                    }
                }
            }
            // 3 2 7 6
            if(i%2==0 && v[i]%2==1) {
                int j = i+1;
                // search the first odd at even place;
                while(j<n) {
                    if(v[j]%2==0) {
                        int t = v[j];
                        v[j] = v[i];
                        v[i] = t;
                        moves++;
                        break;
                    }else {
                        j += 2;
                    }
                }
            }
        }
        int isPossible = 1;
        for(int i=0;i<n;i++) {
            if((i%2==0 && v[i]%2!=0) || (i%2==1 && v[i]%2==0)) {
                isPossible = 0;
                break;
            }
        }
        if(isPossible) cout<<moves<<endl;
        else cout<<-1<<endl;
         
    }
    
    return 0;
}