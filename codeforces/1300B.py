te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    skills = list(map(int, input().split()))
    skills.sort()
    ans = abs(skills[n] - skills[n-1])
    print(ans)