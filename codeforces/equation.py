def isPrime(x):
    # Corner cases
    if (x <= 1):
        return False
    if (x <= 3):
        return False

    # This is checked so that we can skip
    # middle five numbers in below loop
    if (x % 2 == 0 or x % 3 == 0):
        return True
    i = 5
    while(i * i <= x):

        if (x % i == 0 or x % (i + 2) == 0):
            return True
        i = i + 6

    return False


n = int(input())
notFound = True
a = 2
while notFound:
    a = a << 1
    b = n + a
    if isPrime(b):
        notFound = False
print(b, a)
