def solve(v, i, j, n):
    max1 = -1
    min1 = 1000000007
    for k in range(i, j+1):
        if max1 < v[k]:
            max1 = v[k]
        if min1 > v[k]:
            min1 = v[k]
    if max1 - min1 >= j - i + 1:
        return True
    return False


te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    p = list(map(int, input().split()))
    ans = 'NO'
    k1 = 0
    k2 = 0
    for i in range(n-1):
        if abs(p[i+1] - p[i]) >= 2:
            k1 = i+1
            k2 = i+2
            ans = 'YES'
            break
    if ans == 'NO':
        print('NO')
    else:
        print('YES')
        print(k1, k2)
