n = int(input())

while n > 0:
    n -= 1
    s = list(input())
    i = 0
    j = len(s) - 1
    f = [0]*26
    i = 0
    while i < len(s):
        # print(i)
        j = i
        while j+1 < len(s) and s[j+1] == s[i]:
            j += 1
        if (j - i) % 2 == 0:
            f[ord(s[i]) - 97] = 1
        i = j
        i += 1
    ans = ''
    for i in range(26):
        if f[i] == 1:
            ans += chr(97 + i)
    print(ans)
