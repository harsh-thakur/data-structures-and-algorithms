from collections import defaultdict 

maxEle = 201*1000
adj = defaultdict(list)
vu = [False]*maxEle
ivComp = []

n, m = map(int, input().split())


def dfs(nod, weaker, bigger):
    vu[nod] = True
    weaker = min(weaker, nod)
    bigger = max(bigger, nod)
    # print(weaker, bigger)
    for neighbor in adj[nod]:
        if not vu[neighbor]:
            weaker, bigger = dfs(neighbor, weaker, bigger)    
    return weaker, bigger      


for i in range(m):
    u, v = map(int, input().split())
    u -= 1
    v -= 1
    adj[u].append(v)
    adj[v].append(u)

for nod in range(n):
    if not vu[nod]:
        weaker = nod
        bigger = nod
        weaker, bigger = dfs(nod, weaker, bigger)
        ivComp.append((weaker, bigger))
        # print(weaker, bigger)

curEnd = -1
ans = 0
# print(ivComp[0][1])
for comp in ivComp:
    # print(comp[0], comp[1], curEnd)
    if comp[0] <= curEnd:
        ans += 1
    curEnd = max(curEnd, comp[1])
print(ans)
