#include <bits/stdc++.h>
using namespace std;
 
#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    IOS;
    int t; cin>>t;
    while (t--){
        /* code */
        string s; cin>>s;
        int ans;
        int suf0 = 0, suf1 = 0;
        for(int i=0; i<s.size();++i) {
            suf0 += s[i] == '0' ? 1:0;
            suf1 += s[i] == '1' ? 1:0;
        }
        ans = min(suf0,suf1);
        int pref0=0,pref1=0;
        for(int i=0;i<s.size(); ++i) {
            pref0 += (s[i]=='0'), suf0 -= (s[i] == '0');
            pref1 += (s[i]=='1'), suf1 -= (s[i] == '1');
            ans = min(ans, pref1 + suf0);
            ans = min(ans, pref0 + suf1);
        }
        cout<<ans<<endl;
    }
    return 0;
}