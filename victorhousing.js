'use strict';
process.stdin.resume();
process.stdin.setEncoding('utf-8');
let inputString = '';
let currentLine = 0;
process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});
process.stdin.on('end', _ => {
    // inputString = inputString.trim().split('\n').map(string => {
    //     return string.trim();
    // });
    inputString = inputString.split(/\r?\n/);
    solution();  
});

function solution() {
    let storage = inputString[0].split(',');
    let n = storage.length;
    let arr = [];
    let cache = {};
    for(let i=1;i<n;i++) {
        let part = inputString[i].split(" ");
        arr.push([part[0],+(part[2]),part[3]]);
        cache[part[0]] = i-1;
    }
    console.log(cache,arr);
    let unitToConverIn = "";
    let i = 0;
    while(i<n) {
        if(!cache.hasOwnProperty(storage[i])) {
            unitToConverIn = storage[i];
            break;
        }
        i++;
    }
//     Pallet = 12 Rack
// Rack = 18 Row
// Row = 30 Self
    for(let i=0;i<n-1;i++) {
        normalizeInSingleUnit(cache, arr,unitToConverIn,n,i);
    }
    arr = arr.sort((a,b)=>b[1]-a[1]);
    let ansStatement = "";
    ansStatement = "1" + arr[0][0];
    for(i=1;i<n-1;i++) {
        ansStatement += " = " + Math.floor(arr[0][1]/arr[i][1]) + arr[i][0];
    }
    ansStatement += " = " + arr[0][1] + arr[0][2];
    console.log(ansStatement);
}

function normalizeInSingleUnit(cache,arr,unitToConverIn,n,i) {
//     A,B,C,D,E,F
//     C = 10A
// D = 3A
// B = 4C
// E = 4D
// F = 39E
    console.log(i)
    if(arr[i][2] !== unitToConverIn) {
        console.log(arr[i][2],cache[arr[i][2]]);
        arr[i][1] = arr[i][1]*normalizeInSingleUnit(cache,arr,unitToConverIn,n, cache[arr[i][2]]);
        arr[i][2] = unitToConverIn;
    }
    return arr[i][1];
}
