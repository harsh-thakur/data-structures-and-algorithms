#include<bits/stdc++.h>
using namespace std;

#define fast ios_base::sync_with_stdio(false); cin.tie(NULL);
#define Q 101
#define NO_OF_POSSIBLE_CHARACTERS 256

vector<int> doRabinKarp(string s, string p) {
    // cout<<"Rabin Karp called";
    int ns = s.size();
    int np = p.size();
    int hash_p = 0;
    int hash_s = 0;
    int h = 1;
    int i, j;
    vector<int>ans;

    // The value of h would be "pow(hash, np-1)%q" 
    for(i=0; i<np-1; i++) { h = (h*NO_OF_POSSIBLE_CHARACTERS) % Q; }
    
    // Calculate the hash value of pattern and first window of text
    for(i=0; i<np; i++) {
        hash_p = (NO_OF_POSSIBLE_CHARACTERS*hash_p + p[i]) % Q;
        hash_s = (NO_OF_POSSIBLE_CHARACTERS*hash_s + s[i]) % Q;
    }

    // Slide the pattern over text one by one
    for(i=0; i<=ns-np; ++i) {
        if(hash_s == hash_p) {
            for(j=0; j<np; ++j) {
                if(s[i+j] != p[j]) break;
            }
            if(j == np) ans.push_back(i);
        }
        if(i < ns-np) {
            hash_s = (NO_OF_POSSIBLE_CHARACTERS * (hash_s - s[i]*h) + s[i+np]) % Q;
            if(hash_s < 0) hash_s = hash_s + Q;
        }
    }
    return ans;
}

int main() {
    fast
    string s, p; cin>>s>>p;
    vector<int> ans = doRabinKarp(s,p);
    for(int i=0; i<ans.size(); ++i) {
        cout<<ans[i]<<" ";
    }
    return 0;
}

