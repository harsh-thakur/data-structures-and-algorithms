
const str = 'abcd';
let permutations = [];

let swap = async (s, i, j) => {
  return new Promise((res,rej)=>{
    // console.log('swap');
    let temp = s[i];
    s[i] = s[j];
    s[j] = temp;
    res();
  })
  
}

let getAllPermutations = async (s, l, r) => {
  return new Promise(async (resolve, reject) => {
    if (l == r) {
      permutations.push(s);
    }
    for (let i = l; i <= r; i++) {
      //abcd

      // await swap(s, l, i);
      let temp = s[i];
      console.log(s[i],s[l])
      s[i] = s[l];
      s[l] = temp;
      console.log("after swap");
     console.log(l,r,s);

      await getAllPermutations(s, l + 1, r);
      // await swap(s, l, i);
      temp = s[i];
      s[i] = s[l];
      s[l] = temp;
    }
    resolve(permutations);
  })

}

getAllPermutations(str, 0, str.length-1).then((res)=> console.log(res));



// Above solution was not suitable for JavaScript So came up with below alternative which uses same approach


// let getAllPermutations = (str) => {
//     if (str.length < 2 ){
//       return str
//     }

//     let permutations = []

//     for (let i = 0; i < str.length; i++){
//       let current = str[i];

//       if (str.indexOf(current) != i)
//       continue

//       let remaining = str.slice(0, i) + str.slice(i + 1, str.length)

//       for (let permutation of getAllPermutations(remaining)){
//         permutations.push(current + permutation) }
//     }
//     return permutations;
//   }

  // console.log(getAllPermutations(str))