te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    num = list(map(int, input().split()))
    ans = ""
    i = 0
    while i < n:
        # 1 2 3 5 6 8 9 10 11 12 15 17
        a = i
        flag = True
        while flag and i < n-1:
            if num[i+1] == num[i] + 1:
                i += 1
            else:
                flag = False
        b = i
        if b - a >= 2:
            ans = ans + str(num[a]) + '...' + str(num[b])
        else:
            ans = ans + str(num[a])
            if a != b:
                ans = ans + ',' + str(num[b])
        if i != n-1:
            ans += ','
        i += 1
    print(ans)
