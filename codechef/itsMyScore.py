te = int(input())
while te > 0:
    te -= 1
    N = int(input())
    scores = [0]*9
    for i in range(N):
        p, s = list(map(int, input().split()))
        if p <= 8 and scores[p] < s:
            scores[p] = s
    ans = 0
    for i in scores:
        ans += i
    print(ans)
