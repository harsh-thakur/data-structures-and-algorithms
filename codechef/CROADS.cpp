#include<bits/stdc++.h>
using namespace std;

int main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        long n; cin>>n;
        if((n & n-1) == 0) {cout<<-1<<endl; continue;}
        long ans = 0;
        long group = 0;
        // cout<<ceil(log2(n))<<endl;
        for(int i=0; i <= ceil(log2(n)); ++i) {
            int v = n + (1<<i);
            v = v / (1<<(i+1));
            // cout<<i<<" "<<v<<endl;
            ans += (v-1) * (1<<i);
            if(i>0) group += (1<<i);
        }
        ans += group;
        cout<<ans<<endl;
         
    }
    
    return 0;
}