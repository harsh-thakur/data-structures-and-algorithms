#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,mex; cin>>n>>mex;
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>v[i];
        sort(v.begin(), v.end());
        int f[mex] = {0};
        int ans = 0;
        int i;
        // bool isThereLesserElement = 0
        for( i=0;i<n;i++) {
            if(v[i] < mex) {
                f[v[i]]++;
                ans++;
            } else if(v[i]==mex) continue;
            else break;
        }
        int isPossible = 1;
        for(int j=1;j<mex;++j) {
            if(f[j] ==0) {isPossible = 0; break;}
        }  
        if(isPossible) {
            ans += n-i;
            cout<<ans<<endl;
        }else cout<<-1<<endl;
        // cout<<ans<<endl;       
    }
    
    return 0;
}