te = int(input())
while te > 0:
    te -= 1
    n, m = list(map(int, input().split()))
    types = list(map(int, input().split()))
    price = list(map(int, input().split()))
    total = {}
    ans = 1000000
    for i in range(n):
        if total.get(types[i]) is not None:
            total[types[i]] += price[i]
        else:
            total[types[i]] = price[i]
    for i in total:
        if total[i] < ans:
            ans = total[i]
    print(ans)
