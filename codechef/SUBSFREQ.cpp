#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >

#define mod 10000000007
map<int, int>ans;


void printArray(vector<int> sub, int n) { 
    sort(sub.begin(),sub.end());
    map<int,int>m;
    for(int i=0;i<n;i++) {
        m[sub[i]] = (m[sub[i]]%mod + 1)%mod;
    }
    int candidate = sub[0];
    int frequecy  = -1;
    for(int k=0;k<sub.size();++k){
        if(m[sub[k]]>frequecy) {
            frequecy = m[sub[k]]%mod;
            candidate = sub[k];
        }
    }
    ans[candidate] = ((ans[candidate]%mod)+1)%mod;
} 

void printSubsequences(vector<int> arr, int index,  
                       vector<int> subarr) { 
    if (index == arr.size()){ 
        int l = subarr.size(); 
        if (l != 0) 
            printArray(subarr, l); 
    }else{ 
        printSubsequences(arr, index + 1, subarr); 
  
        subarr.push_back(arr[index]);
        printSubsequences(arr, index + 1, subarr); 
    } 
    return; 
} 

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>v(n);
        // for(int i=0;i<n;i++) cin>>v[i];
        // 2 2 3
        ans.clear();
        vector<int>sub;
        map<int,int>m;
        // printSubsequences(v,0,sub);
        unsigned int opsize = pow(2, n); 
        cout<<opsize<<endl;
        // for (int counter = 1; counter < opsize; counter++){ 
        //     sub.clear();
        //     m.clear();
        //     for (int j = 0; j < n; j++){
        //         if (counter & (1<<j)){
        //             sub.push_back(v[j]);
        //             m[v[j]] = (m[v[j]]%mod + 1)%mod;
        //         } 
        //     }
        //     sort(sub.begin(),sub.end());
        //     int candidate = sub[0];
        //     int frequecy  = -1;
        //     for(int k=0;k<sub.size();++k){
        //         if(m[sub[k]]>frequecy) {
        //             frequecy = m[sub[k]]%mod;
        //             candidate = sub[k];
        //         }
        //     }
        //     ans[candidate] = ((ans[candidate]%mod)+1)%mod;
        // } 

        for(int i=1;i<=n;i++) cout<<ans[i]%mod<<" ";
        cout<<endl;
    }
    
    return 0;
}