#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int solve(int n,int k) {
    int x; 
    int steps = INT_MAX;
    int ans = -1;
    while(n--) {
        cin>>x;
        if(k%x==0) {
            if(steps > k/x) {
                steps = k/x;
                ans = x;
            }
        }
    }
    return ans;
}

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,k; cin>>n>>k;
        cout<<solve(n,k)<<endl;
        
    }
    
    return 0;
}