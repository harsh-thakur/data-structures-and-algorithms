// #include <bits/stdc++.h>
// using namespace std;

// int main()
// {
//     int t;
//     cin >> t;
//     while (t--)
//     {
//         /* code */
//         long  x, y, l, r;
//         cin >> x >> y >> l >> r;

//         long Z = -1;

//         if(x == 0 || y == 0)  {
//             Z = 0;
//         } else if (l==0 && r >= 2*max(x,y)) {
//             Z = x | y;
//         } else if ( l == 0) {
//             long xOry = x | y;
//             vector<int>xOryBits;
//             while(xOry) {
//                 if(xOry & 1) xOryBits.push_back(1);
//                 else xOryBits.push_back(0);
//                 xOry = xOry>>1;
//             }
//             long R = r;
//             vector<int>rBits;
//             while(R) {
//                 if(R & 1) rBits.push_back(1);
//                 else rBits.push_back(0);
//                 R = R >> 1;
//             }
//             if(rBits.size() > xOryBits.size()) {
//                 while (rBits.size() != xOryBits.size()) xOryBits.push_back(0);
//             } else if(rBits.size() < xOryBits.size()) {
//                 while (rBits.size() != xOryBits.size()) rBits.push_back(0);
//             }


//         } else {

//         }

//         cout<<Z<<endl;

        
//     }

//     return 0;
// }

// // L=0
// // R≥2⋅max(X,Y)



// // #include <bits/stdc++.h>
// // using namespace std;

// // int main()
// // {
// //     int t;
// //     cin >> t;
// //     while (t--)
// //     {
// //         /* code */
// //         long long x, y, l, r;
// //         cin >> x >> y >> l >> r;
// //         long long ans = 0;
// //         bool isContinued = true;
// //         if (x == 0 || y == 0)
// //         {
// //             cout << 0 << endl;
// //             continue;
// //         }
// //         long long xORy = x | y;
// //         long long m = xORy;
// //         vector<int> M;
// //         while (m)
// //         {
// //             if (m & 1)
// //                 M.push_back(1);
// //             else
// //                 M.push_back(0);
// //             m = m >> 1;
// //         }
// //         int R = r;
// //         vector<int> longestCommonPrefix;
// //         while (R)
// //         {
// //             /* code */
// //             if (R & 1)
// //                 longestCommonPrefix.push_back(1);
// //             else
// //                 longestCommonPrefix.push_back(0);
// //             R = R >> 1;
// //         }
// //         while (M.size() != longestCommonPrefix.size())
// //         {
// //             if (M.size() > longestCommonPrefix.size())
// //                 longestCommonPrefix.push_back(0);
// //             else
// //                 M.push_back(0);
// //         }
// //         vector<int> Z(M.size(), 0);
// //         long long int minimumZ = INT_MAX;
// //         for (int k = M.size() - 1; k >= 0; --k)
// //         {
// //             fill(Z.begin(),Z.end(),0);
// //             if (longestCommonPrefix[k]) {  
// //                 for (int j = k - 1; j >= 0; --j){
// //                     Z[j] = M[j];
// //                 }
// //                 long long int probableZ = 0;
// //                 int mul = 1;
// //                 for (int a = 0; a < M.size(); a++)
// //                 {
// //                     probableZ += (1 << a) * Z[a];
// //                 }
// //                 long long int maxAns = (x & probableZ) * (y & probableZ);
// //                 if(maxAns >= ans && probableZ < minimumZ) {
// //                     ans = maxAns;
// //                     minimumZ = probableZ;
// //                 }
// //             }
// //         }

// //         cout << minimumZ << endl;
// //     }

// //     return 0;
// // }

// // L=0
// // R≥2⋅max(X,Y)



#include<bits/stdc++.h>
using namespace std;

#define ll long long
#define br "\n"
ll x, y, l, r;
ll optimal_z, result;

const ll MX = 1e12;

ll f(ll x,ll y, ll z){
    return (x & z) * (y & z);
} 

void update(ll z) {
    ll cur = f(x,y,z);
    if(cur > result || (cur == result && z < optimal_z)) {
        optimal_z = z;
        result = cur;
    }
}

void solve() {
    cin>>x>>y>>l>>r;
    optimal_z = r;
    result = f(x,y,optimal_z);
    update(l);
    bool is_less = false;
    ll bits_or = (x|y);
    for(ll i = 42; i>=0; --i) {
        ll p = (1ll << i);
        if((r&p) && (is_less || !(l & p) )) {
            ll cur = 0;
            for(ll k = 42; k>i; --k) {
                ll p = (1ll << k);
                if((r & p)) cur |= p;
            }
            bool is_bigger = is_less;
            for(ll k = i-1; k>=0; --k) {
                ll p = (1ll << k);
                if(!(l & p)) {
                    if( bits_or & p ) {
                        cur |= p;
                        is_bigger = true;
                    }
                } else {
                    if(!is_bigger || (bits_or & p)) cur |= p;
                }
            }
            if(cur >= l && cur <= r) {
                update(cur);
            }
            is_less = true;
        }
    }
    cout<<optimal_z<<br;
    // return;
}

int main() {
    int t; cin>>t;
    while (t--){
        solve();
    }
    
    return 0;
}