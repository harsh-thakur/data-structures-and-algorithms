#include<bits/stdc++.h>
using namespace std;

int main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,q; cin>>n>>q;
        string s; cin>>s;
        int f[26] = {0};
        int maxOccurence = 0;
        for(int i=0;i<n;++i) {
            ++f[(int)s[i] - 97];
            if(f[(int)s[i] - 97] > maxOccurence) {
                maxOccurence = f[(int)s[i] - 97];
            }
        }
        // cout<<maxOccurence;
        while (q--){
            /* code */
            int noOfIsolationCenters; cin>>noOfIsolationCenters;
            int ans = 0;
            if(maxOccurence <= noOfIsolationCenters)  ans = 0;
            else {
                for(int i=0;i<26;i++) {
                    if(f[i]>noOfIsolationCenters) ans += f[i] - noOfIsolationCenters;
                }
            }
            cout<<ans<<endl;
        }
        
    }
    
    return 0;
}