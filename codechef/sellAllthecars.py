te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    cars = list(map(int, input().split()))
    cars.sort()
    i = n-1
    ans = 0
    k = 0
    mod = 1e9+7
    while i >= 0:
        if cars[i] - k < 0:
            ans += 0
        else:
            ans = int(ans % mod) + int(cars[i] - k) % mod
        k += 1
        i -= 1
    print(int(ans % mod))
