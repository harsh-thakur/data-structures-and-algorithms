#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int visited[200005], choose[200005],a[200005],b[200005];

vector<vi>adj(200005);
vi cities;
vi res;

void dfs(int u) {
    visited[u] = 1;
    cities.pb(u);
    int i;
    rep(i,adj[u].size()) {
        if(visited[adj[u][i]] == 0 && choose[adj[u][i]] == 1) {
            dfs(adj[u][i]);
        }
    }

}

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,m,i,u,v; cin>>n>>m;
        cities.clear();
        res.clear();
        rep(i,n) {
            adj[i].clear();
            visited[i] = 0;
            choose[i] = 0;
            cin>>a[i];
        }
        rep(i,n) {
            cin>>b[i];
        }
        rep(i,m) {
            cin>>u>>v;
            u--; v--;
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        int num = a[0];
        int den = b[0];
        f(i,1,n) {
            if(a[i]*den > num*b[i]) {
                num = a[i];
                den = b[i];
            }
        }
        rep(i,n) {
            if(a[i]*den == b[i]*num) {
                choose[i] = 1;
            }else {
                choose[i] = 0;
            }
        }
        fd(i,n-1,0) {
            if(visited[i] == 0 && choose[i] == 1) {
                cities.clear();
                dfs(i);
                if(cities.size() > res.size()) {
                    res = cities;
                }
            }
        }
        cout<<res.size()<<endl;
        random_shuffle(res.begin(),res.end());
        rep(i,res.size()) {
            cout<<res[i]+1<<" ";
        }
        cout<<endl;
    }
    
    return 0;
}