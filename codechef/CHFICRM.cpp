#include<bits/stdc++.h>
using namespace std;
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>q(n);
        for(int i=0;i<n;i++) cin>>q[i];
        int fives = 0, tens = 0, fifteens =0;
        bool isPossible = true;
        for(int i=0;i<n;i++) {
            if(q[i] == 5) fives++;
            else if(q[i] == 10) {
                if(fives > 0) {
                    fives--;
                    tens++;
                }else {
                    isPossible = false;
                    break;
                }
            } else if(q[i]==15) {
                fifteens++;
                if(tens>0) {
                    tens--;
                } else if(fives > 1) {
                    fives -= 2;
                } else {
                    isPossible = false;
                    break;
                }
            }
        }
        if(isPossible) cout<<"YES\n";
        else cout<<"NO\n";
         
    }
    
    return 0;
}