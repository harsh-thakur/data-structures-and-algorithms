#include <bits/stdc++.h>
using namespace std;

#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0);
#define endl "\n"
#define int long long
#define f(i, a, b) for (i = a; i < b; i++)
#define rep(i, n) f(i, 0, n)
#define fd(i, a, b) for (i = a; i >= b; i--)
#define pb push_back
#define mp make_pair
#define vi vector<int>

void solve(int n, int k)
{
    vector<int> v(n);
    for (int i = 0; i < n; i++)
        cin >> v[i];
    int count = 0;
    map<int, int> m;
    // if (k == 1)
    // {
        for (int i = 0; i < n; i++)
        {
            if (m[v[i]])
            {
                // cout<<i<<" ";
                count++;
                m.clear();
                m[v[i]] = 1;
            }
            else
            {
                m[v[i]] = 1;
            }
        }
        int ans1 = (count+1)*k;
        // cout<<ans1<<endl;
        // cout << count + 1;
    // } else {
        m.clear();
        count = 0;
        for (int i = 0; i < n; i++){
            if (m[v[i]]){ 
                if(m[v[i]]==1) count++;
                count++;
                m[v[i]]++;
            }
            else{
                m[v[i]] = 1;
            }
        }
        int ans2 = (k + count);
        // cout<<ans2<<endl;
        cout<<min(ans1,ans2);
    // }
}

int32_t main()
{
    IOS int t;
    cin >> t;
    while (t--)
    {
        /* code */
        int n, k;
        cin >> n >> k;
        solve(n, k);
        cout << endl;
    }

    return 0;
}