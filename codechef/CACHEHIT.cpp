#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,b,q;
        cin>>n>>b>>q;
        int l = -1, r = -1;
        int hit = 0;
        while(q--) {
            int x; cin>>x;
            x = x;
            // cout<<l <<" "<<r<<endl;
            if((x>l && x>r) || (x<l && x<r)) {
                hit++;
                int block = x / b;
                l = b*block;
                r = (l+b-1) < n? (l+b-1):n-1;
            }
        } 
        cout<<hit<<endl;
    }
    return 0;
}