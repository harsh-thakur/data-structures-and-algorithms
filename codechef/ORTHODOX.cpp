#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>v[i];
        map<int,int>cache;
        int flag  = 1;
        for(int i=0;i<n;i++) {
            if(cache[v[i]]) {
                flag = 0; break;
            } else {
                // cache.insert({v[i],1});
                cache[v[i]] = 1;
            }
            int OR = v[i];
            for(int j=i+1; j<n;j++) {
                OR = OR | v[j];
                // cout<<OR<<" "<<cache[OR]<<endl;
                if(cache[OR]) {
                flag = 0; break;
                } else {
                    // cout<<"else "<<OR<<endl;
                    cache[OR] = 1;
                    // cout<<"After insertion "<<cache[OR]<<endl;
                }
            }
            if(flag==0) break;
        }
        if(flag) cout<<"YES\n";
        else cout<<"NO\n";
        
    }
    
    return 0;
}