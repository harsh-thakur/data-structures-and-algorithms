def solve(s, i, j, n):
    # print('i ,j, n',i, j)
    count = 0
    for x in range(i+1, n):
        c = s[x]
        y = j + 1
        while y < n:
            if c == s[y]:
                count += 1
                y = n
            else:
                y += 1
        j = j+1
    return count


te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    s = list(input())  # hrashras
    ans = 0
    for i in range(n):
        c = s[i]
        k = 0
        j = i + 1
        i_x = i
        while j < n:
            # print(i, j, c)
            if s[j] == c:
                k += 1 + solve(s, i, j, n)
                j = n
            else:
                j += 1
        if k > ans:
            ans = k
    print(ans)
