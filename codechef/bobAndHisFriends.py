t = int(input())
while t > 0:
    t = t - 1
    f, a, b, c = list(map(int, input().split()))
    floors = list(map(int, input().split()))
    floors.sort()
    # isIndisde = 0
    ans = 0
    if b >= floors[0] and b <= floors[f-1]:
        ans = abs(b-a) + c
    else:
        if b < floors[0]:
            ans = floors[0] - b + abs(floors[0] - a) + c
        elif b > floors[f - 1]:
            ans = b - floors[f-1] + abs(floors[f-1]-a) + c
    print(ans)
