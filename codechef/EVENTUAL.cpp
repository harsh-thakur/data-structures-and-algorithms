#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        string s; cin>>s;
        int cache[26] = {0};
        for(int i=0;i<s.size();++i) {
            cache[s[i]-'a']++;
        }
        int flag = 1;
        for(int i=0;i<26;++i) if(cache[i]%2!=0 ) { flag = 0; break;}
        if(flag) cout<<"YES\n";
        else cout<<"NO\n";
        
    }
    
    return 0;
}