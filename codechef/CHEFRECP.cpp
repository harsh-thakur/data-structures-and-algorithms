#include<bits/stdc++.h>
using namespace std;

int main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;

        vector<int>v(n); for(int i=0;i<n;i++) cin>>v[i];
        map<int, int> m;
        int i=0; int j = 1;
        int ans = 1;
        vector<int>o;
        while(i<n){
            // 1 1 2 2 2 3
            if(!m[v[i]]) {
                j = i+1;
                while(j<n){
                    if(v[i] == v[j]) j++;
                    else break;    
                }
                m[v[i]] = j-i;
                o.push_back(j-i);
                i = j;
            } else {
                ans = 1;
                break;
            }
        }
         if(ans == -1) cout<<"NO\n";
         else {
             sort(o.begin(),o.end());
             for(i=0; i<o.size()-1;i++){
                 if(o[i] == o[i+1]) {ans = -1; break;}
             }
             if(ans == -1) cout<<"NO\n";
             else cout<<"YES\n";
         }
    }
    
    return 0;
}