t = int(input())
while t > 0:
    t -= 1
    N = int(input())
    ans = 0
    for i in range(N):
        s, p, v = list(map(int, input().split()))
        s = s + 1
        t = (p//s)
        t = t*v
        ans = max(t, ans)
    print(ans)
