te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    m = -1
    li = [0]*101
    x = list(map(int, input().split()))
    for i in x:
        li[i] += 1
        if li[i] > m:
            m = li[i]
    print(n - m)
