#include<bits/stdc++.h>
using namespace std;
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        string s; cin>>s;
        int ans = 0;
        int len = s.size();
        for(int i=0;i<len-1;) {
            if(s[i]=='x' && s[i+1]=='y') {
                ans++;
                i+=2;
            } else if(s[i]=='y' && s[i+1]=='x') {
                ans++;
                i+=2;
            } else {
                ++i;
            }
        }
        cout<<ans<<endl;
         
    }
    
    return 0;
}