te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    twos = 0
    zeros = 0
    se = list(map(int, input().split()))
    for i in se:
        if i == 2:
            twos += 1
        if i == 0:
            zeros += 1
    ans = int(twos*(twos-1)/2) + int(zeros*(zeros-1)/2)
    print(ans)
