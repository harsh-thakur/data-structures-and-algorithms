te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    trees = list(map(int, input().split()))
    trees.sort()
    ans = 0
    if n == 1:
        ans = 1
    else:
        if trees[1] - 1 != trees[0]:
            ans += 1
            trees[0] += 1
        i = 1
        while i < n-1:
            if trees[i] - 1 != trees[i-1] and trees[i+1] - 1 != trees[i]:
                ans += 1
                trees[i] += 1
            i += 1
        if trees[n-1] - trees[n-2] != 1:
            ans += 1
    print(ans)
