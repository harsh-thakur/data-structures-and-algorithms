#include<bits/stdc++.h>
using namespace std;

int main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,s; cin>>n>>s;
        vector<int>p(n);
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>p[i];
        for(int i=0;i<n;i++) cin>>v[i];
        int min_zero = 105;
        int min_one = 105;
        int isOne = 0;
        int isZero = 0;
        for(int i=0;i<n;i++) {
            if(v[i]==0 && p[i]<min_zero) { min_zero = p[i];}
            if(v[i]==1 && p[i]<min_one) min_one = p[i];

        }
        // cout<<(min_zero+min_one);
        if(min_one+min_zero+s <= 100) cout<<"yes\n";
        else cout<<"no\n";
        
    }
    
    return 0;
}