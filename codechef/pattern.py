c = "*"
n = int(input())
k = 1
for i in range(1, n+1):
    k = i
    for j in range(1, i+1):
        if j % 2 == 1:
            print(c, end='')
        else:
            print(k, end='')
            k += 1
    print()
