import math
# harsh thakur's solution


def isPrime(n):
    if (n <= 1):
        return False
    if (n <= 3):
        return True
    if (n % 2 == 0 or n % 3 == 0):
        return False
    i = 5
    while(i * i <= n):
        if (n % i == 0 or n % (i + 2) == 0):
            return False
        i = i + 6

    return True


def getprimeFactors(n):
    primes = []
    while n % 2 == 0:
        primes.append(2)
        n = n // 2
    for i in range(3, int(math.sqrt(n))+1, 2):
        while n % i == 0:
            primes.append(i)
            n = n // i
    if n > 2:
        primes.append(n)
    i = 1
    count = 1
    ans = 1
    # print(primes)
    while i < len(primes):
        if primes[i] != primes[i-1]:
            ans = ans*(count+1)
            count = 1
        else:
            count += 1
        i += 1
    return ans - 2


te = int(input())
while te > 0:
    te -= 1
    x, k = list(map(int, input().split()))
    if k == 1:
        print(1)
    elif k == 2:
        if isPrime(x):
            print(0)
        else:
            print(1)
    else:
        if isPrime(x):
            print(0)
        else:
            factors = getprimeFactors(x)
            if factors >= k:
                print(1)
            else:
                print(0)
