te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    cars = list(map(int, input().split()))
    ans = 'YES'
    i = 0
    j = 0
    One = -1
    while i < n and j < n:
        if cars[j] == 1 and (i == j or One == -1):
            i = j
            One = 1
            j += 1
        elif cars[j] == 1 and j-i < 6:
            ans = 'NO'
            break
        elif cars[j] == 1 and j-i >= 6:
            i = j
            j += 1
        else:
            j += 1
    print(ans)