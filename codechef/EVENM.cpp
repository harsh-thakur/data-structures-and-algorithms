#include<bits/stdc++.h>
using namespace std;
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        int matrix[n][n];
        int curr = 1;
        for(int i=0;i<n;i++) {
            if(i%2==0) {
                for(int j=0;j<n;j++) {
                    matrix[i][j] = curr++;
                }
            } else {
                for(int j=n-1;j>=0;j--) {
                    matrix[i][j] = curr++;
                }
            }
        }
        for(int i=0;i<n;i++) {
            for(int j=0;j<n;j++) {
                cout<<matrix[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    
    return 0;
}