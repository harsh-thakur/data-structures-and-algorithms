te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    s = input()
    # print(s[0])
    ans = ''
    i = 0
    while i < n:
        if s[i] == 'L' or s[i] == 'R':
            ans += s[i]
            i += 1
            while True:
                if i < n and (s[i] == 'L' or s[i] == 'R'):
                    i += 1
                else:
                    break
        else:
            # i += 1
            ans += s[i]
            i += 1
            while True:
                if i < n and (s[i] == 'U' or s[i] == 'D'):
                    i += 1
                else:
                    break
    x = 0
    y = 0
    for i in ans:
        if i == 'L':
            x -= 1
        if i == 'R':
            x += 1
        if i == 'U':
            y += 1
        if i == 'D':
            y -= 1
    print(x, y)
