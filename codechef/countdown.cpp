#include<bits/stdc++.h>
using namespace std;

int main() {
    
    int t; cin>>t;
    for(int c =1; c<=t; c++) {
        int n,k; cin>>n>>k;
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>v[i];
        int ans = 0;
        for(int i=0;i<n; i++) {
            if(v[i]==k) {
                int tempK = k-1;
                int j = i+1;
                while(tempK  && j<n) {
                    if(v[j]==tempK) {
                        j++;
                        --tempK;
                    } else { break;};
                }
                i = j-1;
                if(tempK==0) ans++;
            }
        }
        
        cout<<"Case #"<<c<<": "<<ans<<"\n";
    }
    
    return 0;
}