#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >

void solve(int c,int r) {
    int digitsC = c % 9==0 ? c/9 : (c/9 + 1);
    int digitsR = r % 9 == 0 ? r/9 : (r/9 + 1);
    if(digitsC >= digitsR)  cout<<1 << " "<<digitsR;
    else cout<<0<<" "<<digitsC;
}

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int c,r; cin>>c>>r;
        solve(c,r);
        cout<<endl;
        
    }
    
    return 0;
}