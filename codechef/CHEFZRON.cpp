#include <bits/stdc++.h>
using namespace std;

// 3
// 3
// 0 0 0
// 4
// 1 0 1 0
// 5
// 1 1 0 1 0

vector<int> right_solution(int cur_pos, vector<int> v)
{
    int moves = 0;
    int ans_index = cur_pos;
    int runnig_value = 1;
    for (int i = cur_pos+1; i<v.size(); ++i)
    {
        runnig_value += v[i];
        moves++;
        if (runnig_value != 1)
        {
            ans_index = i;
            break;
        }
    }
    if (runnig_value == 1)
    {
        for (int i = 0; i < cur_pos; ++i)
        {
            runnig_value += v[i];
            moves++;
            if (runnig_value != 1)
            {
                ans_index = i;
                break;
            }
        }
    }

    return {ans_index,moves};
}

vector<int> left_solution(int cur_pos, vector<int> v)
{
    int moves = 0;
    int ans_index = cur_pos;
    int runnig_value = 1;
    for (int i = cur_pos - 1; i >= 0; --i)
    {
        runnig_value += v[i];
        moves++;
        if (runnig_value != 1)
        {
            ans_index = i;
            break;
        }
    }
    if (runnig_value == 1)
    {
        for (int i = v.size() - 1; i >= cur_pos; --i)
        {
            runnig_value += v[i];
            moves++;
            if (runnig_value != 1)
            {
                ans_index = i;
                break;
            }
        }
    }

    return {ans_index,moves};
}

int main()
{
    int t;
    cin >> t;
    while (t--)
    {
        /* code */
        int n;
        cin >> n;
        int cnt_one = 0;
        vector<int> v(n);
        for (int i = 0; i < n; i++)
        {
            cin >> v[i];
            if (v[i] == 1)
                cnt_one++;
        }
        int min_possible_moves = 0;
        if (cnt_one == 0)
            min_possible_moves = 0;
        else if (cnt_one == 1)
            min_possible_moves = -1;
        else
        {
            for (int i = 0; i < n; i++)
            {
                if (v[i] == 1)
                {
                    // Escape it;
                    // min (left_solution, right_solution)
                    vector<int> left = left_solution(i,v);
                    vector<int> right = right_solution(i,v);
                    if(left[1] < right[1]) {v[left[0]] += 1; min_possible_moves +=left[1]; }
                    else {v[right[0]] += 1; min_possible_moves += right[1];}
                }
            }
        }
        cout << min_possible_moves << endl;
    }

    return 0;
}