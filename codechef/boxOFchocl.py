te = int(input())
while te > 0:
    te -= 1
    n = int(input())
    W = list(map(int, input().split()))
    x1 = -1
    i1 = -1
    x2 = -1
    i2 = -1
    for i in range(n):
        if W[i] > x1:
            i1 = i
            x1 = W[i]
    for i in range(n):
        if W[i] == x1 and i1 != i:
            i2 = i
            x2 = W[i]
    if x1 == x2:
        ans = 0
        if i2 - i1 < n//2:
            t = n//2 - i1
            i2 += t
            ans += n-i2
            print(ans)
        else:
            print(0)
    else:
        ans = 0
        i1 = n//2
        # print(i1)
        ans += n-i1
        print(ans)
