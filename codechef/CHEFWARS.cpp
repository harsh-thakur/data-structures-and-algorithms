#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >

int solve(int n,int p) {
    if(n<=0) return 1;
    if(p==0) return 0;
    return solve(n-p, p/2);
}

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,p; cin>>n>>p;
        cout<< solve(n,p)<<endl;
    }
    
    return 0;
}