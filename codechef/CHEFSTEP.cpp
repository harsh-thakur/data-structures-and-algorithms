#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,k; cin>>n>>k;
        string ans = "";
        for(int i=0;i<n;i++) {
            int x;
            cin>>x;
            if(x%k==0) {
                ans += "1";
            }else ans += "0";
        }
        cout<<ans<<endl;
    }
    
    return 0;
}