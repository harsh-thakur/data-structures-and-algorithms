te = int(input())


def binaryToDecimal(n):
    dec_value = 0
    base = 1
    j = len(n) - 1
    while j >= 0:
        if n[j] == '1':
            last_digit = 1
        else:
            last_digit = 0
        dec_value += last_digit * base
        base = base * 2
        j -= 1
    return dec_value


def add(A, B):
    # print(A, B)
    ans = 0
    while B > 0:
        ans += 1
        U = A ^ B
        V = A & B
        A = U
        B = V * 2
    return ans


while te > 0:
    te -= 1
    A = input()
    B = input()
    A = binaryToDecimal(A)
    B = binaryToDecimal(B)
    print(add(A, B))
