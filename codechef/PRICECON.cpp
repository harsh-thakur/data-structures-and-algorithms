#include<bits/stdc++.h>
using namespace std;
#define int long long

int32_t main() {
    int t; cin>>t;
    while (t--) {
        /* code */
        int n,p; cin>>n>>p;
        int loss = 0;
        int x;
        for(int i=0;i<n;i++) {
            cin>>x;
            if(x>p) loss += x-p;
        }
        cout<<loss<<endl;
         
    }
    
    return 0;
}