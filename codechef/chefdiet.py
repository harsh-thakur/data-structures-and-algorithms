te = int(input())

while te > 0:
    te -= 1
    n, k = list(map(int, input().split()))
    a = list(map(int, input().split()))
    ans = "YES"
    remainingChoco = 0
    index = 1
    for i in range(n):
        a[i] = remainingChoco + a[i]
        if a[i] < k:
            ans = "NO"
            index = i+1
            break

        remainingChoco = a[i] - k
    if ans == 'YES':
        print(ans)
    else:
        print(ans, index)        