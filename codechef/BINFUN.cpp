#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >
string reverse(string s) {
    int i = 0,j=s.size()-1;
    while(i<=j) {
        char t = s[i];
        s[i] = s[j];
        s[j] = t;
        i++;
        j--;
    }
    return s;
}

string covertToBinary(int x) {
    string binX = "";
    while(x) {
        if(x&1) binX += "1";
        else binX += "0";
        x = x>>1;
    }
    return reverse(binX);
}

int converToDecimal(string s) {
    int dec_value = 0; 

    int base = 1; 
    for(int i=s.size()-1; i>=0; --i) { 
        int last_digit = s[i] == '0' ? 0 : 1; 
        dec_value += last_digit * base; 
        base = base * 2; 
    }
    return dec_value;
}

int BinaryConcatenation(int x,int y) {
    // cout<<x<<" "<<y<<" ======\n";
    string binX = covertToBinary(x);
    string binY = covertToBinary(y);
    // cout<<binX<<" "<<binY<<endl;
    string binXplusY = binX + binY;
    string binYplusX = binY + binX;
    // cout<<binXplusY<<" "<<binYplusX<<endl;

    int XplusY = converToDecimal(binXplusY);
    int YplusX = converToDecimal(binYplusX);
    // cout<<XplusY<<" "<<YplusX<<endl;

    return XplusY - YplusX;
}

int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n;
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>v[i];
        if(n==1) {cout<<v[0]<<endl; continue;}
        int ans = BinaryConcatenation(v[0],v[1]);
        for(int i=0;i<n;i++) {
            for(int j=i+1;j<n;j++) {
                ans = max(ans, BinaryConcatenation(v[i],v[j]));
            }
        }
        cout<<ans<<endl;
    }
    
    return 0;
}