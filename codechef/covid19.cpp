#include<bits/stdc++.h>
using namespace std;

int main() {
    int t; cin>>t;
    while (t--)
    {   int n; cin>>n;
        vector<int>x(n);
        for(int i=0;i<n;i++) cin>>x[i];
        int minAns = INT_MAX;
        int maxAns = INT_MIN;
        int count = 1;
        for(int i=1;i<n;++i) {
            if(abs(x[i]-x[i-1])<=2) {
                count++;
            } else {
                if(minAns>count) minAns = count;
                if(maxAns<count) maxAns = count;
                count = 1;
            }
        }
        if(minAns>count) minAns = count;
        if(maxAns<count) maxAns = count;
        cout<<minAns<<" "<<maxAns<<endl;
        /* code */
    }
    
    return 0;
}