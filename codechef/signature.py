te = int(input())

while te > 0:
    te -= 1
    n, m = list(map(int, input().split()))
    a = [[]]*n
    b = [[]]*n
    for i in range(n):
        a[i] = list(input())
    for i in range(n):
        b[i] = list(input())
    x = []
    for i in range(n):
        for j in range(m):
            if a[i][j] == '1':
                x.push((i, j))
    print(x)