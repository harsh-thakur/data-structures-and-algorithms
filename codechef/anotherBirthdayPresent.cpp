#include<bits/stdc++.h>
using namespace std;

int binarySearch(vector<int>&v,int l,int r,int n) {
    if(l<=r) {
        int mid = l + (r-l)/2;
        if(v[mid] == n) return mid;
        if(v[mid]>n) return binarySearch(v,l,mid-1,n);
        return binarySearch(v,mid+1,r,n);
    }
    return -1;
}
void swap(int *xp, int *yp)  
{  
    int temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
} 

int main() {

    int t; cin>>t;
    // 1≤T≤100
    // 1≤K≤N≤105
    // 1≤Ai≤109 for each valid i
    // the sum of N over all test cases does not exceed 106
    while (t--)
    {
        int n,k; cin>>n>>k;
        vector<int>original;
        vector<int>sorted;
        int x;
        for(int i=0;i<n;i++) {
            cin>>x;
            original.push_back(x);
        }
        int isPossible = 1;

        vector< vector<int>>kLists(k);
        for(int i=0;i<n;i++) {
            int listNo = (i)%k;
            kLists[listNo].push_back(original[i]);
        }
        // how do we merge k Lists;
        for(int i=0;i<k;i++) sort(kLists[i].begin(),kLists[i].end());
        // 1 3 5
        // 2 4
        for(int i=0;i<n;i++) {
            int element = kLists[i%k][ceil(i/k)];
            // cout<<element<<endl;
            sorted.push_back(element);
        }
        for(int i=0;i<n-1;i++) {
            if(sorted[i]>sorted[i+1]) {
                isPossible = 0;
                break;
            }
        }

        if(isPossible) cout<<"yes\n";
        else cout<<"no\n";
        /* code */
    }
    

    return 0;
}