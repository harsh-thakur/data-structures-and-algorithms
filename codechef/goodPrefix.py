te = int(input())
while te > 0:
    te -= 1
    s = input()
    n = len(s)
    k, x = list(map(int, input().split()))
    # 3
    # abcdefagahai
    # 0 1
    # abcdefagahai
    # 1 1
    # abcdefagahai
    # 2 1
    m = {}
    m[s[0]] = 1
    i = 1
    y = k
    while i < n:
        if not m.get(s[i]):
            m[s[i]] = 1
        elif m[s[i]] < x:
            m[s[i]] += 1
        elif m[s[i]] >= x and k > 0:
            k -= 1
        else:
            break
        i += 1
    ans = i - y - k
    for i in m:
        if m[i] <= x:
            pass
        else:
            ans = 0
            break
    print(ans)
