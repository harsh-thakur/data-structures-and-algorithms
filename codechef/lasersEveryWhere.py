# Authour : Harsh Thakur

te = int(input())
while te > 0:
    te -= 1
    n, q = list(map(int, input().split()))
    points = list(map(int, input().split()))
    while q > 0:
        q -= 1
        x1, x2, y = list(map(int, input().split()))
        # p1 = Point(x1, y)
        # q1 = Point(x2, y)
        ans = 0
        i = x1-1
        while i < x2-1:
            # print(i)
            if(i+1 == x2 and points[i] == y) or (x1 == i+2 and y == points[i+1]):
                i += 1
                continue
            if points[i] <= y and points[i+1] >= y or points[i] >= y and points[i+1] <= y:
                ans += 1
            i += 1
        print(ans)
