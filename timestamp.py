import datetime as dt

keys = {
    'Sunday': 0,
    'Monday': 1,
    'Tuesday': 2,
    'Wednesday': 3,
    'Thursday': 4,
    'Friday': 5,
    'Saturday': 6
}


def getDayCode(y, m, d):
    d = dt.date(year=y, month=m, day=d)
    day = d.strftime("%A")
    return keys[day]


def ifInSameWeek(date1, date2):
    d1 = dt.date(year=int(date1[0]), month=int(date1[1]), day=int(date1[2]))
    d2 = dt.date(year=int(date2[0]), month=int(date2[1]), day=int(date2[2]))
    noOfDays = d2 - d1
    print(noOfDays.days)
    # if noOfDays < 7:
    #     return True
    return noOfDays.days < 7


# ts = list(map(str, input().split()))
ts = [
    '2019-01-01',
    '2019-01-02',
    '2019-01-08',
    '2019-02-01',
    '2019-02-02',
    '2019-02-05',
]
ts.sort()
values = []
ans = []
for i in ts:
    k = i.split('-')
    print(k)
    value = getDayCode(int(k[0]), int(k[1]), int(k[2]))
    values.append(value)
i = 0
t = []
print(values)
while i < len(values)-1:
    t.append(ts[i])
    if values[i] < values[i+1]:
        if not ifInSameWeek(ts[i].split('-'), ts[i+1].split('-')):
            # t.append(ts[i])
            ans.append(t)
            t = []
    else:
        ans.append(t)
        t = []
    i += 1
if len(t) != 0:
    if values[len(values)-2] < values[len(values)-1]:
        if not ifInSameWeek(ts[len(values)-2].split('-'), ts[len(values)-1].split('-')):
            ans.append(t)
            ans.append([ts[len(values)-1]])
        else:
            t.append(ts[len(values)-1])
            ans.append(t)
else:
    ans.append([ts[len(values)-1]])
print(ans)

# ts = [
#     '2019-01-01',
#     '2019-01-02',
#     '2019-01-08',
#     '2019-02-01',
#     '2019-02-02',
#     '2019-02-05',
# ]
