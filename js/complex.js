
const promiseA = Promise.resolve("Promise A Ok");
promiseA.then((result) => {
    const promiseB = Promise.resolve("Promise B Ok");
    const promiseC = Promise.resolve("Promise C Ok");
    Promise.all([promiseB,promiseC]).then(ok=>{
        console.log(ok);
        callPromiseD();
       
    })
    
}).catch((err) => {
    
});

function callPromiseE(params) {
    return new Promise((resolve, reject)=>{
        // reject();
        resolve();
    })
}


function callPromiseD() {
    console.log('promise d called');
    
    const promiseD = Promise.resolve('Promise D resolved');
    promiseD.then((res) => {
        console.log(res);
        callPromiseE().then(()=> console.log('Promise E resolved')).catch(e=> callPromiseD());
    })
}