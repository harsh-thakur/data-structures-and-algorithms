let object = [{
    '2019': {
        'jan': {
            x:{'data': '1010'}
        },
        'feb': {
            'data': '1012'
        },
        'march': {
            'data': '1013'
        }
    },
    '2020': {
        'jan': {
            'data': '1010'
        },
        'feb': {
            'data': '1014'
        },
        'march': {
            'data': '1011'
        }
    }
}
]


// output = [{
//     2019.jan.data : 1010,
//   2019.feb.data : 1012,
//   2019.march.data : 1013
// },
// {
//     2020.jan.data : 1010,
//   2020.feb.data : 1014,
//   2020.march.data : 1011
// }
// ]

function unwind(obj, key, result) {
    if(typeof obj !== 'object') {
        result[key] = obj;
        return result;
    }
    const keys = Object.keys(obj);
    for(let i=0;i<keys.length; ++i) {
        unwind(obj[keys[i]],key +'.'+keys[i], result);
    }
    return result;
}



function main(input) {
    let output = [];
    for(let i=0;i<input.length;i++) {
        if(typeof input[i] === 'object') {
            let keys = Object.keys(input[i]);
            for(let j=0;j<keys.length;j++) {
                let result = {};
                let key = keys[j];
                output.push(unwind(input[i][key], key,result));
            }
        } else output.push(input[i]);
    }
    console.log(output);
}

main(object)

// output = []
// object.map((e) => {
//     Object.keys(e).map((q) => {
//         obj = {}
//         key = q + '.'
//         Object.keys(e[q]).map((yy) => {
//             key = key + yy + '.'
//             Object.keys(e[q][yy]).map((zz) => {
//                 key = key + zz
//                 obj[key] = e[q][yy][zz]
//                 key = q + "."
//             })
//         })
//         output.push(obj)
//     })
// })
// console.log(output);
