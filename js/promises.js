const sleep = ms => {
    // Return a new promise
    // No need defining the executor function with a `reject` callback
    return new Promise(resolve => {
      // Pass resolve as the callback to setTimeout
      // This will execute `resolve()` after `ms` milliseconds
      setTimeout(resolve, ms);
    });
  }
  const executeOperation = () => {
    console.log("executed");
    
}

  // Sleep for 5 seconds
// Then execute the operation
sleep(5000);


// Delay function
// Using async/await with sleep()
const delay = async (callback, seconds = 1) => {
  // Sleep for the specified seconds
  // Then execute the operation
  await sleep(seconds * 1000);
  callback();
}



// Using the `delay()` function
// Execution delayed by 5 seconds
delay(executeOperation, 5);


const timing = callback => {
    // Get the start time using performance.now()
    const start = Date.now();
    console.log(`Start: ${start}`);
    
    // Perform the asynchronous operation
    // Finally, log the time difference
    return Promise.resolve(callback())
      .finally(() => console.log(`Timing: ${Date.now() - start}`));
  }

const asyncOperation = () => new Promise(resolve => {
    setTimeout(() => resolve('DONE'), 5 * 1000);
  });
  
  // Compute execution time in ms
  // And log it to the console
  timing(asyncOperation); 