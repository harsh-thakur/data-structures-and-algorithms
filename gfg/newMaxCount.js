// const inputArray = [1, 2, 2, 3, 1, 2];

// function pickNumbers(v) {
//     let occurences = {};
//     let n = v.length;
//     for(let i=0; i<n; ++i) {
//         if(occurences.hasOwnProperty(v[i])) occurences[v[i]]++;
//         else occurences[v[i]] = 1;
//     }
//     let ans = 0;
//     for(let element in occurences) {
//         if(occurences.hasOwnProperty(+(element)+1)) {
//             ans = ans > occurences[element] + occurences[+(element)+1] ? ans : occurences[element] + occurences[+(element)+1];
//         }
//     }
//     return ans;
// }

// let ans = pickNumbers(inputArray);
// console.log(ans);


function removeAll(str,k) {
    let stack = [];
    let n = str.length;
    for(let i=0;i<n;i++) {
        let currentCharacter = str[i];
        if(stack.length > 0 && stack[0].count === k) {
            let characterAtTop = stack[0].character;
            while(stack.length>0 && stack[0].character === characterAtTop) stack.shift();
        }

        if(stack.length>0 && stack[0].character=== currentCharacter) {
            stack.unshift({
                character:currentCharacter,
                count: stack[0].count+1
            });
        } else stack.unshift({
            character:currentCharacter,
            count: 1
        });
    }
    if(stack.length>0 && stack[0].count===k) {
        let characterAtTop = stack[0].character;
        while(stack.length>0 && stack[0].character === characterAtTop) stack.shift();
    }

    let ans = "";
    for(let i=stack.length-1;i>=0;--i) ans += stack[i].character;

    return ans;
}

let ans = removeAll('abbbcd', 3);
console.log(ans);