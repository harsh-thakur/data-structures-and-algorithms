#include <bits/stdc++.h>
using namespace std;

#define IOS                  \
    ios::sync_with_stdio(0); \
    cin.tie(0);              \
    cout.tie(0);
#define endl "\n"
#define int long long
#define f(i, a, b) for (i = a; i < b; i++)
#define rep(i, n) f(i, 0, n)
#define fd(i, a, b) for (i = a; i >= b; i--)
#define pb push_back
#define mp make_pair
#define vi vector<int>

int pickNumbers(vector<int> &v){
    cout<<"called";
    int n = v.size();
    map<int, int>occurences;
    for(int i=0;i<n;i++) {
        if(occurences[v[i]]) occurences[v[i]]++;
        else occurences[v[i]] = 1;
    }
    map<int,int>:: iterator itr;
    int ans = 0;
    for(itr = occurences.begin(); itr != occurences.end(); ++itr) {
        int element = itr->first;
        if(occurences[element+1]!=0) ans = max(ans, occurences[element] + occurences[element+1]);
    }
    return ans;
}

int32_t main()
{
    IOS int n;
    cin >> n;
    cout<<n;
    vector<int> v(n);
    for (int i = 0; i < n; i++)
        cin >> v[i];
    cout << pickNumbers(v);
    return 0;
}