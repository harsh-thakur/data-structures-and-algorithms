#include<bits/stdc++.h>
using namespace std;

#define IOS ios::sync_with_stdio(0); cin.tie(0); cout.tie(0);
#define endl "\n"
#define int long long
#define f(i,a,b) for(i=a;i<b;i++)
#define rep(i,n) f(i,0,n)
#define fd(i,a,b) for(i=a;i>=b;i--)
#define pb push_back
#define mp make_pair
#define vi vector< int >


int32_t main() {
    IOS
    int t; cin>>t;
    while (t--) {
        /* code */
        int n; cin>>n; 
        vector<int>v(n);
        for(int i=0;i<n;i++) cin>>v[i];
        int k; cin>>k;
        int lessThanOrK = 0;
        for(int i=0;i<n;i++) if(v[i]<=k) lessThanOrK++;
        // cout<<"lessThanOrK "<<lessThanOrK<<endl;
        int ans = INT_MAX;
        int moreThank =0 ;
       for(int i=0;i<lessThanOrK;i++)  if(v[i]>k) moreThank++;
       ans = min(moreThank,ans);
       // 2 1 5 6 3
    //    cout<<ans<<endl;
       int i =1;
       while(i <= n-lessThanOrK) {
           if(v[i-1] > k) moreThank--;
           if(v[i-1+lessThanOrK] > k) moreThank++;
           ans = min(moreThank,ans);
            ++i;
       }
    cout<<ans<<endl;
    }
    
    return 0;
}